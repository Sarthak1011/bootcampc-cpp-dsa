#include<iostream>

class Parent {

	public:
	Parent(){
		std::cout << "In Constructor"<<std::endl;
	}
	~Parent(){
		std::cout << "In Parent Destructor"<<std::endl;
	}
};
class Child : Parent {

	public:
	Child(){
		std::cout << "In Child Constructor"<<std::endl;
	}
	~Child(){
		std::cout << "In Child Destructor"<<std::endl;
	}
	void* operator new(size_t size){
		std::cout << "new"<<std::endl;
		void *ptr = malloc(size);
		return ptr;
	}
	void operator delete(void *ptr){
		std::cout << " delete "<<std::endl;
		free(ptr);
	}

};

int main(){

	Child *obj = new Child();
	delete obj;
}

