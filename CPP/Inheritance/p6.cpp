#include<iostream>

class Parent {

	int x = 10;
	int y = 20;
	
	public :

	Parent(){

		std::cout << "In No args Constructor"<<std::endl;
	
	}
	Parent(int x , int y){
		this -> x = x;
		this -> y = y;
		std::cout << "Parent Para Constructor "<<this->x << std::endl;
	}


	void getData(){
		std::cout << x << std::endl;
		std::cout << y << std::endl;
	}
};
class Child :public Parent {
	int x = 30;
	public :

	Child(int x , int y , int z){
		Parent(this->x,y);

		std::cout << "Child Constructor"<<this <<std::endl;

	}
	void printData(){
		std::cout << x << std:: endl;
	}
};
int main(){

	Child obj(10,20,30);
	obj.getData();
	obj.printData();
	return 0;
}


