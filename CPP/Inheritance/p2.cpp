#include<iostream>

class Parent {

	int x = 10;
	
	protected :

	int y = 20;

	public :

	int z = 10;

	Parent(){
		std::cout << "In Parent constructor"<<std::endl;
	}
	Parent(int x ,int y ,int z){
		this->x = x;
		this->y = y;
		this->z = z;
		std::cout << "In Parent Para constructor"<<std::endl;
	}
	void getData1(){
		std::cout << x  << y  << z<< std::endl;
	}
};

class Child :public Parent {
	public:
	Child(int x , int y , int z){
	Parent(x , y , z);
		std::cout << "In Child constructor"<<std::endl;
	}
	void getData(){

		getData1();
	}
};

int main(){
	Child obj(40,50,60);
	obj.getData();
	std::cout <<obj.y<<std::endl;

	return 0;
}


