#include<iostream>

class Parent {

	int x =  10;
	public:
	Parent (){

		std::cout << "In Parent Constructor "<<std::endl;

	}
	void getData(){

		std::cout <<" Parent x" << x << std::endl;

	}
};

class Child :public  Parent {

	int y = 20;

	public :

	Child(){
	
		std::cout << "In Child Constructor"<< std::endl;

	}
	void printData(){

		std::cout << "In Child y "<< y << std::endl;
		
	}
};

int main(){

	Child obj;
	obj.printData();
	obj.getData();
	return 0;
}

