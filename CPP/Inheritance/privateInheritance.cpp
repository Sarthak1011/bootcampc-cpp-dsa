#include<iostream>

class Parent {

	int x = 10;
	
	protected :
	int y = 20;

	public:

	int z = 30;

	Parent(){

		std::cout << "In Parent Constructor "<< std::endl;

	}
};

class Child : private Parent {
	//x is privatly inherited and scope is private 
	//y is privatly inherited and scope is private 
	//z is privatly inherited and scope is private   
	public:
	Child(){

		std::cout << "In Child COnstructor "<< std:: endl;
		std::cout <<  y << z << std::endl;

	}
};

int main(){

	Child obj;
	std::cout << obj.x << obj.y << obj.z << std::endl;

	return 0;
}

