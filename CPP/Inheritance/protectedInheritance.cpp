//protectedInheritance 


#include<iostream>

class Parent {

	int x = 10;
	 protected :

	int y = 20;

	 public :
	int z = 30 ;

	Parent (){

		std::cout << "In Parent Constructor "<< std::endl;

	}
};
class Child : protected Parent {
	//x is protectly inherited and scope is private 
	//y is protectly inherited and scope is protected 
	//z is protectly inherited and scope is protected  
	//
	public :
		Child(){

			std::cout << "In Child Constructor "<< std::endl;
			//std::cout << x << y << z<<std::endl;
		}
};
int main(){

	Child obj;
	std::cout <<obj.x << obj.y << obj.z << std::endl;
	return 0;
}

	
