#include<iostream>

class Employee {
	std::string empName = "Sarthak";
	int empId = 10;
	public:
	Employee(){
	
		std::cout << "Employee Constructor "<< std::endl;

	}
	void getInfo(std::string empName , int empId){
		this -> empName = empName;
		this -> empId = empId;
		std::cout << empName << " " << empId << std::endl;
	}
	~Employee(){
		std::cout << "emp Destructor "<<std::endl;
	}
};
class Company {
	std ::string comName = "Veritas";
	int empStrength = 1234;
	Employee obj;

	public:
	Company(std::string comName , int empStrength){
		std::cout << "Company Constructor "<< std::endl;
		this->comName = comName;
		this->empStrength= empStrength;
		//Employee obj1(empName,empId);
	}
	void getInfo(std::string empName , int empId){
		obj.getInfo(empName , empId);
		std:: cout << comName << " " << empStrength << std::endl;
	}
	~Company(){
		std::cout << "com Destructor"<< std::endl;
	}
};

int main(){

	Company obj("pubmatic" , 4321);
	obj.getInfo("Nikita",10);
	return 0;
}

