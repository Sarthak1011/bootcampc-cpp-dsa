//public Inheritance 
#include<iostream>

class Parent {

	int x = 10;

	protected :

	int y = 20;

	public :

	int z = 20;

	Parent (){

		std::cout << "In Parent Constructor "<< std::endl;

	}
};

class Child :public Parent {
	//x is publicly inherited but scope is private 
	//y is publicly inherited but scope is protected 
	//z is publicly inherited but scope is public 

	public :
	Child(){
		std::cout << "In Child Constrctor"<<std::endl;
		std::cout << x <<" " <<y << " "<< z <<std::endl; // x is private with in this context
	}
};

int main(){

	Child obj;
	// x and y  are private and protected within this context simultanously 
	std::cout << obj.x << obj.y << obj.z<<std::endl;
	return 0;
}
