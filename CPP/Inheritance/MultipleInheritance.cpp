#include<iostream>

class Parent1 {

	int x = 10;

	public :

	Parent1(){
		std::cout << "In Parent1 Constructor"<<std::endl;
	}
	virtual void putData(){

		std::cout << "In Parent1 getData"<<x<<std::endl;

	}
};

class Parent2 {

	int x = 20;

	public:
	Parent2(){

		std::cout << "In Parent2 Constructor"<<std::endl;

	}
	virtual void putData(){

		std::cout << "In Parent2 PrintData"<<x << std::endl;

	}
};

class Child :public Parent1 , public Parent2 {

	int x = 30;

	public :
	Child(){
	
		std::cout << "In Child Constructor"<< std::endl;

	}
	void putData(){

		std::cout << "In child putData"<< x << std::endl;

	}
};

int main(){

	Child obj;
	obj.putData();//Say which specific Parent print data 
	obj.Parent1::putData();
	(Parent2(obj)).putData();
	return 0;
}
