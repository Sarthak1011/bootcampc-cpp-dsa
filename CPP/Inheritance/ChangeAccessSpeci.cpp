#include<iostream>

class Parent {

	int x = 10;

	protected :
	int y = 20;

	public: 
	int z = 30;
	
	void getData(){

		std::cout << "In Parent GetData"<<std::endl;

	}
};

class Child : public Parent {

	using Parent::getData;

	public :
	using Parent :: y;

	void getData() = delete;

};

int main(){

	Child obj;

	std::cout << obj.y << obj.z <<std::endl;
	obj.getData();
	return 0;
}

