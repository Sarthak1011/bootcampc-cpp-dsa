#include<iostream>

class Parent {

	int x = 10;

	public :

	Parent(){

		std::cout << "Parent"<<std::endl;

	}
	 void getData(){

		std::cout << "Parent x"<<x << std::endl;
	}
};

class Child :public Parent {

	int x = 10;

	public :

	Child(){
		std::cout << "Child "<< std::endl;
	}
	void getData(){
		std::cout << "Child x" << x <<std::endl;
	}
};

int main(){

	Parent *obj= new Child();//jyacha reference tyachi method call if we write virtual for parent method then child method is call
	obj->getData();
	return 0;
}
	
