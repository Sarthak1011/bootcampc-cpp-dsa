#include<iostream>

class Parent {

	int x = 10;
	int y = 20;

	public :

	int a = 30;
	 Parent(){

		 std::cout << "In Parent Constructor "<<this<< std::endl;

	 }
	 ~Parent(){

		 std::cout << "In Parent Destructor" <<this << std::endl;

	 }
	 void printData(){

		 std::cout << x << " " << y << " " << a <<std::endl;

	 }
};

class Child : public Parent {

	int z = 40;

	public :

	Child(){
	
		std::cout << "In Child Constructor "<<this<< std:: endl;

	}
	~Child(){

		std::cout << "In Child Destructor "<< this <<std::endl;

	}	
	void getData(){

		std::cout << a << " " << z <<std::endl;

	}
};

int main(){

	Parent obj1 ;
	obj1.printData();
	Child obj2 ;
	obj2.printData();
	obj2.getData();
}
