#include<iostream>

class Parent {

	int x = 10;

	public :

	Parent(){

		std::cout << "In Parent "<<std::endl;

	}
	friend std:: ostream& operator<< (std::ostream& out , const Parent& obj){

		out << "In Parent Friend"<<std::endl;
		out << obj.x;
		return out;
	}
	Parent (Parent& obj){
		std::cout << " Parent Copy"<<std::endl;
	}
};

class Child : public Parent {

	int x = 20;

	public :

	Child(){
	
		std::cout << "In Child Constructor"<< std::endl;

	}
	friend std::ostream& operator << (std::ostream& out ,const Child& obj){
		out << "In Child Friend"<<std::endl;
		out << obj.x;
		return out ;
	}
	Child (Child& obj){
		std::cout << "Child Copy"<<std::endl;
	}
};

int main(){

	Child obj1;
	std::cout << (const Parent)obj1 << std::endl;//copy constructor 
	std::cout << (const Parent&)obj1 <<std::endl;//not copy constructor 

	Child obj2;
	std::cout << obj2 <<std::endl;
	return 0;

}


