#include<iostream>

class Parent {

	int x = 10;

	public :

	Parent(){

		std::cout << "Parent"<<std::endl;

	}
	 void getData(){

		std::cout << "Parent x"<<x << std::endl;
	}
	 Parent(Parent& obj){
		 std::cout << "copy"<< std::endl;
	 }
};

class Child :public Parent {

	int x = 10;

	public :

	Child(){
		std::cout << "Child "<< std::endl;
	}
	void getData(){
		//parent function call type 1
		Parent::getData();
		std::cout << "Child x" << x <<std::endl;
	}
};

int main(){

	Child obj;
	//type 1
	obj.getData();
	//type 2
	obj.Parent::getData();
	//type 3 heavy operation
	(Parent(obj).getData());
	return 0;
}
	
