#include<iostream>

class Demo {
	int x = 10;
	int y = 20;

	public:

	Demo(){

		std::cout << "No Args" << std::endl;

	}

	Demo(int x , int y){
		this -> x = x;
		this -> y = y;

		std::cout << "Para-Constructor" << std::endl;
	}
	
       	Demo(Demo &xyz){
	 
	 	 std::cout << "Copy Constructor"<<std::endl;

 	 }
};

int main() {

	Demo obj1;//No Args--->Demo obj{}  
	Demo obj2(1000,2000);//Para Constructor
	Demo obj3 = obj1;//Copy Constructor

//	Demo obj();//Method Declaration
	Demo obj4//No Args;
	obj4 = obj1;//
	return 0;
}
