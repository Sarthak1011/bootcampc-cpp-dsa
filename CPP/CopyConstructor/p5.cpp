/*
1. Copy Constructor is bydefault constructor
2. Copy Constructor is used for thr Replica of object
3. if we call copy constructor then call constructor outside the class 
4. we do not use the this for calling the copy constructor because this i pointer in cpp
5. we call copy constructor using 
         1. main function (Constructor parameter should be refernce)
	 2. creating object in construtor and pass (constructor parameter should be a refernce )
	 3. pass object to the member function(member function parameter should be class obj not a refernce)
6.we use copy constructor ass a notification
7.if we call copy construtor and we not wrritten this constructor then is statisfy but no args and para constructor we not wrriten and we call it then it errror
 
Demo obj();method declaration
Demo obj{};Constructor call
 */ 

#include<iostream>

class CopyConstructor {

	public :

		CopyConstructor(){
			
			// Type 1 --> Creating object in constructor and pass it 
		 	CopyConstructor obj(10 , 20);
			CopyConstructor obj1(obj);

		}
		CopyConstructor( int x , int y ){
		
			std::cout << "para" << std::endl;
	
		}

		CopyConstructor(CopyConstructor& obj){

			std::cout<< " in copy Constructor " << std::endl;

		}
		void Access (CopyConstructor &  obj){
		
			std::cout << "Access" << std::endl;

		}
};


int main(){

	CopyConstructor obj;
	//Type 2 -->creating object in main function and pass object then call the copy constructor 
	CopyConstructor obj1(obj);
	obj.Access(obj1);

}

