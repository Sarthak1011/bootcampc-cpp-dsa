#include<iostream>

class Demo {

	public:
		int x = 10;
	Demo(){
		
		std::cout<<"No args"<<std::endl;
		std::cout<< sizeof(this) <<std::endl;

	}

	Demo(int x){

		std::cout<<"Para "<<x<<std::endl;
		std::cout<<this <<std::endl;

	}
	Demo( Demo &x ){//Demo *x = &obj1

		std::cout<<"Copy"<<std::endl;
		std::cout<<this <<std::endl;

	}
};

int main(){

	Demo obj1;//no args
	Demo obj2(10);//para
	Demo obj3(obj1);//Copy
	Demo obj4 = obj1;//obj4(obj1)//copy
	return 0;
}
