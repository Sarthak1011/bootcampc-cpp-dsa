#include<iostream>

class CopyConstructor {

	int x = 10;
	int y = 20;

	public :

	CopyConstructor() {
		CopyConstructor obj(x , y);
		CopyConstructor obj1(obj);//calling copy constructor in constructor
		std::cout << "In No Args"<<std::endl;

	}
	CopyConstructor(int x , int y){

		std::cout << "In Para"<<std::endl;

	}
	CopyConstructor(CopyConstructor &obj){

		std::cout << "In copy"<<std::endl;

	}
	CopyConstructor access(CopyConstructor obj1){

		std::cout << "call copy constructor in member function"<<std::endl;
		return obj1;
	}

};

int main(){

	CopyConstructor obj1;
	CopyConstructor obj2 = obj1;//main function call the copy Constructor
	
	CopyConstructor obj3 = obj1.access(obj2);

	CopyConstructor arr[]={obj1,obj2,obj3};


}

