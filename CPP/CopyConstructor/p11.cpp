/*
 if you retrun refernce then parameter should be a refernce

 */

#include<iostream>
class Demo{

	public:
	Demo (){
		std::cout<<"no"<<std::endl;
	}
	Demo (int x){
		
		std::cout<<"para"<<std::endl;
	}
	Demo(Demo &obj){
		
		std::cout<<"copy"<<std::endl;
	}
};
int main(){
	
	Demo obj1;
	Demo obj2;
	Demo obj3;

	Demo arr[] = {obj1,obj2,obj3};
	/*
	 Demo arr[0] = obj1;
	 Demo arr[1] = obj2;
	 Demo arr[2] = obj3;
	 */ 
	return 0;
}

