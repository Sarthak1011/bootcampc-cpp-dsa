#include<iostream>

class Demo {

	public :
	int x = 10;

		Demo(){
		
			std::cout << " No Args " <<std::endl;

		}

		Demo(int x){

			std::cout <<  " Para " <<std::endl;
		
		}
		Demo(Demo &obj){

			std::cout <<" Copy "<<std::endl;

		}

		void fun(){


			std::cout << x <<std::endl;
			std::cout << this ->x <<std::endl;
		}
};
int main (){

	Demo obj1;
	Demo obj2(obj1);

	std::cout << obj1.x <<std::endl;//10
	std::cout << obj2.x <<std::endl;//10
	
	obj1.x = 50;

	std::cout << obj1.x <<std::endl;//50
	std::cout << obj2.x <<std::endl;//10
}
