/*
 if we give values to the parameters then that values are considered ass a local variable means thier is no parameter to the function 
 assign values to the function from left to right 
 */ 
#include<iostream>

class Demo{

	//private 
	int x = 10;
	int y = 20;

	public :

	Demo () {
	
		std::cout<< "No" <<std::endl;

	}

	Demo(int x =10 ,int y =200 ){

		this -> x = x;
		this -> y = y;

		std::cout<< x <<"  "<<y <<std::endl;

	}
	void info(Demo obj){

	}
};

int main(){

//	Demo obj1;error because ambiguous 

	Demo obj2(100);
	return 0;
}


