/*
 IN cpp exception handling if we throw any datatype then in throw block perfect match meet neccesary if no match then run time error terminate called after throwing instance char Aborted
 (...)---> Ellipes
 */ 
#include<iostream>

int main(){

	std::cout<<"start main"<<std::endl;

	try {
		throw 'A';
	}catch(int x){
		std::cout << "Exception Occured"<<std::endl;
	}
	std::cout << "End main "<<std::endl;
	return 0;
}
