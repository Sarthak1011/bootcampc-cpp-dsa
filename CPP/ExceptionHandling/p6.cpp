 #include<iostream>


void searchEle(int arr[],int size , int ele){

	int flag = 0;
	for(int i =0;i<size;i++){
		if(arr[i]==ele){
			flag = 1;
		}
	}
	if(flag == 0)
		throw -1;
	else
		std::cout<<"Ele found"<<std::endl;

}

int main (){

	int size;
	std::cout << "Enter the array size" <<std::endl;
	std::cin>>size;

	int arr[size];

	std::cout<<"Enter the array elements"<<std::endl;

	for(int i =0;i<size;i++){
		std::cin>>arr[i];
	}
	
	int ele ;
	std::cout << "Enter the seaching element"<<std::endl;
	std::cin>>ele;

	try {

		searchEle(arr,size,ele);
	}catch(int i){
		std::cout<<"ele not found"<<std::endl;
	}
}
