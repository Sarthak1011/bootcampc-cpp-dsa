#include<iostream>

class Parent {

	public :

		 virtual void getData() {

			std::cout << "In Parent getData"<<std::endl;

		}
};

class Child final : public Parent {

	public :

		void getData() override{

			std::cout << "In Child getData"<<std::endl;

		}
};
/*class Child2 :public Child {
//cannot derive from final base
};*/

int main(){

	Parent *obj = new Child();
	obj->getData();
//	std::cout << obj->_vptr<<std::endl;
	return 0;
}
