#include<iostream>

class Parent {

        public :

                Parent(){

                        std::cout << "In parent Construtor"<< std::endl;

                }
                virtual void getData(){

                        std::cout << "In Parent getData"<<std::endl;

                }
};

class Child : public Parent {

        public :
                Child(){

                        std::cout << "In child Constructor "<<std::endl;

                }
             /*virtual*/void getData(){

                        std::cout << "In Child getData"<<std::endl;

                }
};

int main(){

        Parent *obj = new Child();
        obj->getData();
        return 0;
}
