//method signature in overriding


#include<iostream>

class Parent {
	public :
		Parent(){
		
			std::cout << "In Parent Constructor"<<std::endl;

		}

		virtual void getData(int x){

			std::cout << "Parent getData"<<std::endl;

		}
		virtual void printData(float x){
		
			std::cout << "Parent PrintData"<<std::endl;
	
		}
};

class Child :public Parent {
 	 
	public :
	 
		Child(){
			std::cout << "In Child Constructor"<<std::endl;
		}
		
		void getData(short int x ){
			std::cout << "In child getData"<<std::endl;
		}
		void printData(float x ){
			std::cout << "In Child printData"<<std::endl;
		}
};

int main(){

	//Heap section
	Parent * obj = new Child();
	obj->getData(10);
	obj->printData(10.5f);

	//stack frame
	Child obj1;
	Parent * obj2 = &obj1;
	obj2->getData(10);
	obj2->printData(10.5f);

	//reference 
	Child obj3;
	Parent& obj4 = obj3;
	obj4.getData(10);
	obj4.printData(10.5f);


	return 0;
}

