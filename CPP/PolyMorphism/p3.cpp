//primitive return type or covarient return type

#include<iostream>

class Parent {

	public:
		Parent(){
			std::cout << "In Parent "<<std::endl;
			std::cout << this <<std::endl;
		}
		virtual Parent *getData(){
			std::cout <<"Parent getData"<<std::endl;
			return new Parent();
			//return this;
		}
};
class Child :public Parent {
	public :
		Child(){
			std::cout << "In child"<<std::endl;
			std::cout << this <<std::endl;
		}
		Parent *getData(){
			
			std::cout << "Child getData"<<std::endl;
			return new Child();
			//return this;
		}
};
int main(){

	Parent *obj = new Child();
	Parent *obj1 = obj->getData();
	std::cout << obj << std::endl;
	std::cout << obj1 <<std::endl;
	return 0;

}

		
