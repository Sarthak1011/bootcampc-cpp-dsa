#include<iostream>
#include<list>

int main(){

	std::list<int> lst ={10,20,30,40,50};
	std::list<int>::iterator itr;

	for(itr = lst.begin();itr!=lst.end();itr++){
		std::cout << *itr << std::endl;
	}
	lst.push_front(11);

	std::cout << "push front"<<std::endl;
	for(itr = lst.begin();itr!=lst.end();itr++){
		std::cout << *itr << std::endl;
	}
	lst.pop_front();

	std::cout << "pop front"<<std::endl;
	for(itr = lst.begin();itr!=lst.end();itr++){
		std::cout << *itr << std::endl;
	}
	lst.push_back(51);

	std::cout << "push back "<<std::endl;
	for(itr = lst.begin();itr!=lst.end();itr++){
		std::cout << *itr << std::endl;
	}
	lst.pop_back();

	std::cout << "pop back"<<std::endl;
	for(itr = lst.begin();itr!=lst.end();itr++){
		std::cout << *itr << std::endl;
	}

	
	std::list<int> lst1 ={50,10,30,10,20};
	std::list<int> lst2 ={100,200,300,400,500};

	std::cout << "splice lst1"<<std::endl;
	//lst2.splice(itr , lst1);
	for(itr = lst1.begin();itr!=lst1.end();itr++){
		std::cout << *itr << std::endl;
	}
	std::cout << "splice lst2"<<std::endl;
	for(itr = lst2.begin();itr!=lst2.end();itr++){
		std::cout << *itr << std::endl;
	}

	std::cout << "remove"<<std::endl;
	lst1.remove(10);
	for(itr = lst1.begin();itr!=lst1.end();itr++){
		std::cout << *itr << std::endl;
	}
	
	lst1.unique();
	std::cout << "Unique"<<std::endl;

	for(itr = lst1.begin();itr!=lst1.end();itr++){
		std::cout << *itr << std::endl;
	}

	std::cout << "merge"<<std::endl;
	lst1.merge(lst2);
	for(itr = lst1.begin();itr!=lst1.end();itr++){
		std::cout << *itr << std::endl;
	}
	lst1.sort();
	std::cout << "sort"<<std::endl;
	for(itr = lst1.begin();itr!=lst1.end();itr++){
		std::cout << *itr << std::endl;
	}
	lst1.reverse();
	std::cout << "reverse"<<std::endl;
	for(itr = lst1.begin();itr!=lst1.end();itr++){
		std::cout << *itr << std::endl;
	}



return 0;

}
