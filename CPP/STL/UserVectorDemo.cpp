#include<iostream>

#include<vector>

class CrickPlayer {

	std::string pName;
	int jerNo;

	public :
		
		CrickPlayer(std::string pName , int jerNo){
			this->pName = pName;
			this->jerNo = jerNo;
		}
		void getInfo(){
			std::cout << pName <<" : "<<jerNo<<std::endl;
		}
};
int main(){
/*
	CrickPlayer obj1("Surya",63);
	CrickPlayer obj2("Rohit",45);
	CrickPlayer obj3("Tilak",30);
*/
	CrickPlayer *obj1 = new CrickPlayer("Surya",63);
	CrickPlayer *obj2 = new CrickPlayer("Rohit",45);
	CrickPlayer *obj3 = new CrickPlayer("Tilak",30);

	std::vector<CrickPlayer> vobj = {*obj1 ,*obj2 ,*obj3};

	for(int i = 0;i<vobj.size();i++){
		vobj[i].getInfo();
	}
}


