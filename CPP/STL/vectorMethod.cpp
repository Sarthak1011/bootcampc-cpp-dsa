#include<iostream>
#include<vector>

int main(){

	std::vector<int> vect = {10,20,30,40};
	std::vector<int> ::iterator itr;

	//begin()  end()
	for(itr = vect.begin();itr!=vect.end();itr++){
		std::cout << *itr <<std::endl;
	}

	//push_back
	vect.push_back(10);
	std::cout << "push_back" <<std::endl;
	for(itr = vect.begin();itr!=vect.end();itr++){
		std::cout << *itr <<std::endl;
	}
	vect.pop_back();
	std::cout << "pop_back" <<std::endl;
	for(itr = vect.begin();itr!=vect.end();itr++){
		std::cout << *itr <<std::endl;
	}
	//shrink_to_fit()   resize()   capacity()   size()
	vect.shrink_to_fit();
	std::cout <<"Shrink_to_fit -> "<< vect.size()<<std::endl;
	vect.resize(10);
	std::cout <<"resize()  ->"<< vect.capacity()<<std::endl;
	vect.shrink_to_fit();
	std::cout <<"Shrink_to_fit -> "<< vect.capacity()<<std::endl;

	//insert()
	std::cout <<"insert()"<<std::endl;
	std::cout << *(vect.insert(itr,200))<<std::endl;

	for(itr = vect.begin();itr!=vect.end();itr++){
		std::cout << *itr <<std::endl;
	}
	//assign()
	std::cout << "assign "<<std::endl;
//	vect.assign(4,500);
	for(itr = vect.begin();itr!=vect.end();itr++){
		std::cout << *itr <<std::endl;
	}
	//at()  front()  back()  data()
	std::cout <<"at"<< vect.at(0)<<std::endl;
	std::cout <<"front"<< vect.front()<<std::endl;
	std::cout <<"back"<< vect.back()<<std::endl;
	std::cout <<"data"<<*(vect.data())<<std::endl;

return 0;
}
