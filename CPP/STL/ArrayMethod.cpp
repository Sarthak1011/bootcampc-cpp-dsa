#include<iostream>
#include<array>

int main(){


	std::array<int , 5> arr={10,20,30,40,50};

	std::array<int, 5> ::iterator itr;

	//begin()  end()
	for(itr =arr.begin();itr!=arr.end();itr++){
		std::cout<< *itr <<std::endl;
	}
	
	//size()  max_size empty()
	std::cout << arr.size()<<std::endl;
	std::cout << arr.max_size()<<std::endl;
	std::cout << arr.empty()<<std::endl;

	//operator[]()  at()  front()  back()  data()
	std::cout << arr.operator[](2)<<std::endl;//30
	std::cout << arr.at(2)<<std::endl;//30
	std::cout << arr.front()<<std::endl;//10
	std::cout << arr.back()<<std::endl;//50
	std::cout << *(arr.data())<<std::endl;//10
					      

	arr.fill(1);

	for(itr =arr.begin();itr!=arr.end();itr++){
		std::cout<< *itr <<std::endl;
		
	}

	std::array<int,5> arr1 ={1,2,3,4,5};
	std::array<int,5> arr2 ={10,20,30,40,50};
	arr1.swap(arr2);
	
	for(itr =arr1.begin();itr!=arr1.end();itr++){
	
		std::cout<< *itr <<std::endl;

	}	
	
	for(itr =arr2.begin();itr!=arr2.end();itr++){

		std::cout<< *itr <<std::endl;
	}	

	return 0;
}
