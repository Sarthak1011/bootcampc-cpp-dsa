
#include<iostream>
#include<vector>

int main(){

//Modifiers
	//void assign(size , value);
	std::vector<int> v1 ={10,20,30};
	std::vector<int> v={10,20,30,40,50};
	//v1.assign(5,10);
	std::vector<int>::iterator itr;
	for(itr = v1.begin();itr!=v1.end();itr++){

		std::cout <<" assign:\t "<< *itr << std::endl;

	}
	//void push_back
	v1.push_back(40);
	for(itr = v1.begin();itr!=v1.end();itr++){
		std::cout << *itr << std::endl;
	}
	//void pop_back
	v1.pop_back();

	//Iterator = Insert(iterator ,ele )
	std::cout << "Insert " << *(v1.insert(itr,100))<<std::endl;
	for(itr = v1.begin();itr!=v1.end();itr++){
		std::cout << *itr << std::endl;
	}
	//Iterator = erase(itrator pos)
	std::cout << "erase"<<std::endl;
	std::cout << *(v1.erase(v1.begin()+1))<<std::endl;//20

	//void swap()
	//v.swap(v1);
	std::cout << "v vector"<<std::endl;
	for(itr = v.begin();itr!=v.end();itr++){
		std::cout << *itr << std::endl;
	}
	std::cout << "v1 vector"<<std::endl;
	for(itr = v1.begin();itr!=v1.end();itr++){
		std::cout << *itr << std::endl;
	}
	v1.clear();

	//1.reference at(position)
	
		std::cout <<" at:\t"<< v.at(2) << std::endl;//30
	
	//2.reference back

		std::cout << " back:\t"<< v.back() <<std::endl;//50
	//3.reference front
		std::cout << " front:\t"<< v.front() <<std::endl;//10

	//4.reference operator[](int pos)
	
		std::cout <<"Operator :\t"<<v.operator[](2)<<std::endl;	//30
	//5.int *data()
		std::cout << " Data \t"<< *(v.data())<< std::endl;//10

//Capacity
		
	//1.int size()	
		std::cout << " size:\t"<< v.size()<< std::endl;//5

	//2.int capacity()
		std::cout << " capacity:\t"<< v.capacity()<< std::endl;//5

	//3.int max_size()	
		std::cout << " maxsize:\t"<< v.max_size()<< std::endl;//19digit size
	//4.void resize(size)
	       // v.resize(10);
		std::cout << " resize:\t"<< v.size()<< std::endl;//10

	//5.bool empty()
		std::cout << "empty:\t"<<v.empty() <<std::endl;//0
	//6.void shrik_to_fit()
		std::cout << "Shrink_to_fit:\t"<<v.size()<<std::endl;//10	
//Iterator 

	//1.iterator begin   2.iterator end()
		std::cout << "iterator" <<std::endl;
		std::vector<int> ::iterator itr1;
		for(itr1 = v.begin();itr1!=v.end();itr1++){
		
			std::cout << *itr1 <<std::endl;
	
		}

		itr1 = v.begin()+2;
		std::cout <<" begin()+2:\t"<<(*itr1) <<std::endl;//30
								 
	//3.const iterator cbegin()   4.const iterator cend()	
		std::cout << "const_iterator" <<std::endl;

		std::vector<int> ::const_iterator itr2;
		for(itr2 = v.cbegin();itr2!=v.cend();itr2++){
		
			std::cout << *itr2 <<std::endl;
	
		}
	//5.reverse iterator rbegin()  6. reverse iterator rend()	
		std::cout << "reverse_iterator" <<std::endl;

		std::vector<int>::reverse_iterator itr3;
		for(itr3 = v.rbegin();itr3!=v.rend();itr3++){
		
			std::cout << *itr3 <<std::endl;
	
		}
	//7.const reverse iterator crbegin()	 8.const reverse iterator crend()
		
		std::vector<int> obj={1,2,3,4};
		std::cout << "const_reverse_iterator" <<std::endl;

		std::vector<int>::const_reverse_iterator itr4;
		for(itr4 = v.crbegin(); itr4!=v.crend(); itr4++){
		
			std::cout << *itr4 <<std::endl;
	
		}
		*/
	return 0;
}

