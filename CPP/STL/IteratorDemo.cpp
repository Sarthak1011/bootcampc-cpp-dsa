#include<iostream>
#include<vector>

int main(){

	std::vector<int> v = {10,20,30,40,50};

	std::vector<int> ::iterator itr;

	for(itr = v.begin();itr!=v.end();itr++){
		std::cout << *itr << std::endl;
	}

	for(auto itr1 = v.begin();itr1!=v.end();itr1++){
		std::cout << *itr1 << std::endl;
	}
	for(auto itr = v.rbegin();itr!=v.rend();itr++){
		std::cout << *itr << std::endl;
	}
	return 0;
}

