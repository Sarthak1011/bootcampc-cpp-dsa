#include<iostream>

class Demo {
	int x = 10;
	int y = 20;

	public :

	Demo (int x , int y){
		this -> x = x;
		this -> y = y;
	}
	friend int operator *(const Demo& obj1 , Demo& obj2 ){

		return obj1.x * obj2.y;

	}
};

int main(){

	Demo obj1(10,50);
	Demo obj2(20,60);

	std::cout << obj1 * obj2 <<std::endl;
}
