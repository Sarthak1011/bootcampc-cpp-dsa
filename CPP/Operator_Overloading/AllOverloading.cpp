/*In operator overloading three way to overload 1.using friend function
						2.using member fuction
						3.using Normal function

if we overload operator using member function then thier is only one argument require because of the member function is related to class and first parameter is hidden this pointer thats why we need only one parameter  
if we use member function then passing argument first argument is complusory class type becz of this pointer obj1 + obj2 == member function 
obj1 + 100 == mamber function priority     100 + obj1 == friend function/normal function priority 
using member function we cannot overload io operator means insertion and extraction becz of first parameter is ostream& which another class 
*/						

#include<iostream>

class Demo{
	int x = 10;
	int y = 20;
	public:
	Demo(int x){
		this -> x = 100;
		this -> y = 20;
	}
	//friend function
	friend int operator % (const Demo& obj1 ,const Demo& obj2){

		std::cout << "In Friend Function"<<std::endl;
		return obj1.x % obj2.y;

	}
	//Member function
	int operator %(const Demo& obj2){
	
		std::cout << "In Member function "<<std::endl;
		return this->x % obj2.y;

	}
	//for accessing private variables
	int getData() const {

		return x;

	}
};

//Normal function
int operator %(const Demo& obj1 ,const Demo& obj2){

	return obj1.getData()%obj2.getData();

}

int main(){

	Demo obj1(10);
	Demo obj2(20);
	std::cout << obj2%obj1 << std :: endl;
}
