/*
 there are three types to overload the operators 1.using friend function 2.using normal function 3.using the member function
           OVERLOAD THE ARITHMATICAL OPERATORS 
	   USING FRIEND FUNCTION

	   cpp madhe jith operator asl tr te tya function la kartat an mg cpu an register kad jatat
	   pn c madhe direct cpu an register kad jatat tyamul overloading chi concept nahi
	   operator overloading madhe call implicitly jato 

 */ 

#include<iostream>

class Demo {

	int x = 10;

	public:
		Demo(int x){
			this -> x = x;

			std::cout<< x << std::endl;

		}
		int getData()const{
			return x;
		}

		//friend int operator+(const Demo& obj1, const Demo& obj2);
};

int operator+(const Demo& obj1 ,const Demo& obj2 ){

	return obj1.getData()+obj2.getData();
}

/*int operator+(const Demo& obj1 , const Demo& obj2){

	return obj1.x + obj2.x;//ostream& operator+(int , int);
}*/

int main(){

	Demo obj1=100;//Demo(&obj1,100);-->Demo(Demo *this,int);
	Demo obj2=200;//Demo(&obj2,200);-->Demo(Demo *this,int);
	
	std::cout<< obj1 + obj2 <<std::endl;
	
	return 0;
}
	
