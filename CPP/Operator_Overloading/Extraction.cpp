#include<iostream>

class Demo {

	int x = 10;
	int y = 20;

	friend std :: istream& operator >>(std::istream& in , Demo& obj){//here do not write const becz we take input from the user we change the data 
  		in >> obj.x;
  		in >> obj.y;
		return in;
	}
	public:
	void printData(){
		std::cout << x << " " << y << std::endl;
	}

};

int main(){

	Demo obj1;

	std::cout << "Enter the value"<<std::endl;

	std::cin >> obj1;
	obj1.printData();
}
