//Insertion Operator

#include<iostream>

class Demo{

	int x = 10;
	int y = 20;
	public:
	Demo(int x){
		this -> x = 100;
		this -> y = 20;
	}
	//friend function
	
	friend int operator / (const Demo& obj1 , const Demo& obj2){

		std::cout << "In Friend Function"<<std::endl;
		return obj1.x/obj2.y;

	}
	//Member function
	int operator /(const Demo& obj2){
	
		std::cout << "In Member function "<<std::endl;
		return this->x/obj2.y;

	}
	//for accessing private variables
	int getData() const {

		return x;

	}
};
/*
//Normal function
int operator /(const Demo& obj1 ,const Demo& obj2){

	return obj1.getData()/obj2.getData();

}
*/
int main(){

	Demo obj1(10);
	Demo obj2(20);
	std::cout << 100/obj1 << std :: endl;
}
