#include<iostream>

class Demo {

	int x = 10;
	public:
	//using friend function
	friend int operator++( Demo& obj){//here we cannot write const becz we change the data
		return ++obj.x;
	}
	int operator++(){
		return ++x;
	}

	int getData(){
		return ++x;
	}	

};
/*
int operator ++(Demo& obj){
	 return obj.getData();
}*/

int main(){

	Demo obj;
	std::cout << ++obj <<std::endl;
}
