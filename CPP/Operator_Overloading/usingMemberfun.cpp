/*
 Operator overloading using member function
class madhe  constructor an member function la pahila parameter hidden this pointer jato  tyamul tyala ekech parameter la dyava lagto 
 */ 
#include<iostream>

class Demo {

	int x = 10;
	public:

		Demo(int x){
			this -> x = x;
		}
		int operator + (const Demo& obj2) {//aplyala jer this ne change karun nasl dyaych tr method constant karaich- 
			this -> x = 40;
			return this->x + obj2.x;
		}
		void getData(){
			std::cout << x << std::endl;
		}
};
int main(){
	Demo obj1(20);
	Demo obj2(30);

	std::cout << obj1 + obj2 <<std::endl;//obj1.operator + (obj2);
	obj2.getData();
	return 0;
}
