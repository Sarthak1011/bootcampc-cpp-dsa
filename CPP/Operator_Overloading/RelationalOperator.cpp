#include<iostream>

class Demo {

	int x;
	int y;

	public :
		Demo(int x , int y){
			this ->x = x;
			this ->y = y;
		}
		//friend function
		friend int operator <(const Demo& obj1, const Demo& obj2){
			return obj1.x < obj2.y && obj1.x < obj2.y;
		}
		//accessing private variable x
		int getDataX()const {
			return x;
		}
		//accessing private variable y
		int getDataY()const {
			return y;
		}
		//member function
		int operator <(const Demo& obj1){
			return this -> x > obj1.y;
		}
};
/*
//normal function
int operator <(const Demo& obj1, const Demo& obj2){
	return obj1.getDataX()<obj2.getDataY();
}
*/
int main(){

	Demo obj1(10 , 20);
	Demo obj2(200 , 100);
	std::cout << (obj1 <obj2) << std::endl;

}
