#include<iostream>


class Demo {

	int x = 10;
	int y = 20;
	
	public :
	Demo (int x){
		this -> x = x;
	}
	friend int operator +(const Demo obj1 , const Demo obj2);
	
	friend int operator +(const Demo obj1 , const int data){
	
		std::cout << "there"<<std::endl;
		return obj1.x + data;
	
	}
};
int operator +(const Demo obj1 , const Demo obj2){
	std::cout << "Here"<<std::endl;
	return obj1.x + obj2.x;
}

int main(){

	Demo obj1(10);
	Demo obj2(20);
	Demo obj3 =(Demo)70;

	std::cout <<obj1 + obj2 <<std::endl;//implicitly call
	std::cout << obj1 + 20 <<std::endl;//typecast primittive datatype into class
}
