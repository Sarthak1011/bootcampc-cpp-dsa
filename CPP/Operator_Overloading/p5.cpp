//OPERATOR + 
#include<iostream>

class Demo {

	int x = 10;
	friend int operator +(Demo& obj1 , Demo& obj2);
};
int operator + (Demo & obj1 , Demo& obj2){
	return obj1.x + obj2.x;
}

int main(){

	Demo obj1;
	Demo obj2;
	std::cout << obj1 + obj2 <<std::endl;
}

		
