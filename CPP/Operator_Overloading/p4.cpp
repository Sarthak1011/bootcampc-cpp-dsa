//                           OPERATOR OVERLOADING 
//std::cout<< obj <<std::endl;s2  
//ostream& operator<<(ostream& cout , int obj.x);
//
//OPERATOR << 
#include<iostream>

class Demo {

	int x = 10;

	public :

		Demo(){

		}
		friend std::ostream& operator<<(std::ostream& cout , Demo obj);
};

std::ostream& operator<<(std::ostream& cout , Demo obj){
	std::cout << obj.x;
	return cout;
}

int main(){

	Demo obj1;

	std::cout << obj1 << std::endl;

}
