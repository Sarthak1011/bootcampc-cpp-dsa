#include<iostream>

class Demo {

	int x = 10;

	protected :

	int y = 20;

	public :

	Demo(){

		std::cout<<"In constructor"<<std::endl;

	}
	void getData(){

		std::cout<< x << y << std::endl;
	}
	friend void accessData(const Demo &obj);
};
//protocol is that friend function has read only access do not change the values thats why make it ass a const 
//friend function has compulsory parameter is refernce
//frined function is used for the access the private and protected variables and function
//
void accessData(const Demo &obj){
	int temp = obj.x;
	//obj.x =obj.y;
	//obj.y = temp;
	std::cout<<obj.x << obj. y << std::endl;

}
int main(){

	Demo obj1;
	obj1.getData();
	accessData(obj1);
}

