/*
 Q.Why we use friend function?
 ->private and protected variable and function we cannot access outside the class thats why we access or read the prvite data we use the friend function

 */ 

#include<iostream>

class Demo {

	int x = 10;

	protected:

	int y = 20;

	public:
	Demo(){
		
		std::cout<<"In Constructor"<<std::endl;

	}
	void getData(){

		std::cout<< x <<std::endl<< y <<std::endl;

	}
	friend void accessData(const Demo& obj);
};
class Demo1{
 
	Demo1 obj1;
 	 void accessData(const Demo1& obj1){

		std::cout<<obj1.x<<std::endl<<obj1.y<<std::endl;

	}
};

int main(){
	Demo obj;
	obj.getData();
	obj.getData();
	return 0;
}

