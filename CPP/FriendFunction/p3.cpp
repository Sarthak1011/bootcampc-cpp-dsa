///friend private chalt because it is not member of this class
//firend function at a time two class cha friend asu shakto
//here object is is constant that why getdata() is make const
#include<iostream>
class Two;//forward declaration

class One {

	int x = 10;

	protected:
	int y = 20;

	public:

	One(){
		std::cout << "One Constructor " <<std::endl;
	}
	private:
	void getData() const {

		std::cout << x <<std::endl;
		std::cout << y <<std::endl;
	}
	friend void accessData(const One& obj1, const Two& obj2);
};
class Two {
	int a = 10;

	protected :
	int b = 20;

	public :

	Two() {
		std::cout << " two Constructor "<<std::endl;
	}

	private:
	void getData() const {

		std::cout << a <<std::endl;
		std::cout << b <<std::endl;
	}

	friend void accessData(const One& obj1, const Two& obj2);
};

void accessData(const One &obj1 ,const Two &obj2 ){
	obj1.getData();
	obj2.getData();
}

int main(){
	One obj1;
	Two obj2;
	accessData(obj1,obj2);
	return 0;
}
	


