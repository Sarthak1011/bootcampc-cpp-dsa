//c++98,c++3,c++11,c++14,c++17,c++20,c++23
//z{30} is standerdized in c++11
//when our code run on another version then use g++ -std=c++03 p1.cpp
#include<iostream>

int main(){

	int x =10;//copy initilization
	int y(20);//direct initilization

	std::cout<<x<<" "<<y<<std::endl;

	int z{30};//uniform initilization--> part of list initilation 

	std::cout<< z <<std::endl;

	return 0;
}
