#include<iostream>

int main(){

	int x = 10;
	int y = 20.5f;

	std::cout<<x<<" "<<y << std::endl;

	int p(10.4f);
	std::cout << p << std::endl;
	
/*
	int a{10};
	int b {10.5f};//error 

	std::cout << a << " " << b << std::endl;*/
	return 0;
}

/*
 p6.cpp:11:21: error: narrowing conversion of ‘1.05e+1f’ from ‘float’ to ‘int’ [-Wnarrowing]
   11 |         int b {10.5f};
      |                     ^
      uniform initilization not support the int b {10.4f}
      but copy intilization support the int y = 20.4f  but its output is 20
      but direct intilization support the int p = 20.4f  but its output is 20
 */ 
