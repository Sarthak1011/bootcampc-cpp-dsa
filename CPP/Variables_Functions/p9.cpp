#include<iostream>

int main(){

	int x = 10;
	int y = 20;

	std::cout<<x<<std::endl;//10
	std::cout<<y<<std::endl;//20

	const int *ptr = &x;

	std::cout<<*ptr<<std::endl;//10
	ptr = &y;

	std::cout<<*ptr<<std::endl;//20

//	*ptr = 20;error read only data

//	std::cout<<x<<std::endl;

	int const  * const ptr1 = &x;
//	*ptr1 =&y;//read only data
//	*ptr1 = x;//error read only data

}

