#include<iostream>

class Demo {

	public:
		int x =10;
		Demo (){
		
			this -> x = 80;
			std::cout<<"In Constructor"<<std::endl;
			std::cout<< x <<std::endl;

		}
		//const void getData();-->error
		void getData ()const  {

			//this ->x = 100;error
			std::cout<< x <<std::endl;

		}
};

int main(){

	const Demo obj;
	obj.getData();

	//obj.x=50;//error
	//obj.getDat.0a();
	return 0;
}
