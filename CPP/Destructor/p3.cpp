/*
 
 *
 */
 
#include<iostream>

class Demo {
	int * ptrArray = NULL;
	public:
	Demo(){
		ptrArray = new int[50];

		std::cout << "In Constructor"<<std::endl;
	}
	~Demo(){
		delete[] ptrArray;
		std::cout << "In Destructor"<<std::endl;
	}
};
int main(){
	{
		Demo obj;
	}
	return 0;
}

