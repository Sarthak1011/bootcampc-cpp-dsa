/*
 if we call one constructor to another then complusory obj is created proof is if object is created then call the destructor
 destructor is used for the close the connections
 */ 
#include<iostream>


class Demo {
	public:
		int x =10;
		Demo (){
			std::cout<<"In COnstructor"<<std::endl;
			std::cout<< x << std::endl;
		}
		Demo(int x){
			this ->x = x;
			std::cout<<"In Constructor para"<<std::endl;
			std::cout<< x << std::endl;
			Demo();
		}
		void getData(){
			std::cout <<x <<std::endl;
		}
		~Demo(){
			std::cout << "Destrcutor"<<std::endl;
		}
};

int main(){

	Demo obj(50);
	std::cout<<"End main"<<std::endl;
	return 0;
}

