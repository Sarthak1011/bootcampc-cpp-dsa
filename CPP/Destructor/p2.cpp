/*
 Destructor is same ass finally  
 object is end then destructor is call
 Destructor is notify that object is end
 heap section data must be free its compulsory then and then call the destructor

 Delete internally call the free
 new internally call the malloc
 heap section object implicitly not deleted 
 jya jya gosti heap ver astat tyach gostiver delete chalto 
 */ 

#include<iostream>

class Demo{
	public :
		Demo(){
			std::cout<<"In constructor"<<std::endl;

		}
		~Demo(){

			std::cout<<"In destructor"<<std::endl;

		}
};

int main(){
	Demo obj1;
	Demo *obj2 = new Demo();
	
	std::cout<<"End main"<<std::endl;
	delete obj2;
	//delete obj1; error because only heap section data apply  delete call
	return 0;
}
