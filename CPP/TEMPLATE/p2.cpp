#include<iostream>
class Demo{
	public:
	void fun(int x,int y){

		std::cout<< " x = " << x << " y = " << y << std::endl;
	}
};
int main(){

	Demo obj;
	obj.fun(10,20);
	obj.fun(10.5f,0.5f);
	obj.fun(10.2,20.3);
	obj.fun('A','B');
	return 0;
}
