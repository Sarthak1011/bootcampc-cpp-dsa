//bubble sort using liked list
//


#include<stdio.h>
#include<stdlib.h>


typedef struct BubbleSort{
	int data;
	struct BubbleSort *next;
}sort;

sort *head=NULL;

sort *createNode(){
	sort *newNode=(sort *)malloc(sizeof(sort));
	printf("enter the data\n");
	scanf("%d",&newNode->data);

	newNode->next=NULL;
	return newNode;
}
void addNode(){
	sort *newNode=createNode();

	if(head==NULL){
		head=newNode;
	}else{
		sort *temp=head;
		while(temp->next!=NULL){
			temp=temp->next;
		}
		temp->next=newNode;
	}
}
int printLL(){
	if(head==NULL){
		return -1;
	}else{
		sort *temp=head;
		while(temp->next!=NULL){
			printf("|%d|->",temp->data);
			temp=temp->next;
		}
			printf("|%d|\n",temp->data);
	}
}
void bubbleSort(){
	sort *temp1=head,*temp2=temp1->next;
	while(temp1->next!=NULL){
		if(temp1->data > temp2->data){

		int temp=temp1->data;
		temp1->data=temp2->data;
		temp2->data=temp;
		temp1=temp1->next;
		}
	}
}
void main(){
	int nodeCount;
	printf("enter the count of the nodes\n");
	scanf("%d",&nodeCount);
	for(int i=0;i<nodeCount;i++){
		addNode();
		printLL();
	}
	bubbleSort();

	printf("sorted linked list is\n");
	printLL();
}


