//

#include<stdio.h>

int mystrlen(char *str){
	int count=0;
	while(*str!='\0'){
		str++;
		count++;
	}
	return count;
}

void sort(char *str[],int size){

	for(int i=0;i<size;i++){
		for(int j=0;j<size;j++){
			if(mystrlen(str[i])>mystrlen(str[j+1])){
				int temp=*str[j];
				*str[j]=*str[j+1];
				*str[j+1]=temp;
			}
		}
	}
}

void main(){
	char *str[]={"kahna","ashish","rahul","raj","anuj","shashi"};
	for(int i=0;i<6;i++){
		puts(str[i]);
	}

	mystrlen(str);

	sort(str,6);
	for(int i=0;i<6;i++){
		puts(str[i]);
	}

}
