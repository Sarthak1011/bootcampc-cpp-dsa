#include<stdio.h>
#include<stdlib.h>

typedef struct TreeNode {
	int data;
	struct TreeNode *left;
	struct TreeNode *right;
}tree;

tree *createNode(int level){

	tree *newNode = (tree *)malloc(sizeof(tree));

	level++;
	printf("Enter the data of %d level\n",level);
	scanf("%d",&newNode->data);

	getchar();
	printf("do you want to add the left subtree? At %d\n",level);
	char ch;
	scanf("%c",&ch);

	if(ch=='y'||ch=='Y'){
		newNode->left=createNode(level);
	}else{
		newNode->left=NULL;
	}

	printf("Do you want to add right subtree? At %d\n",level);
	getchar();
	scanf("%c",&ch);

	if(ch=='Y'||ch=='y'){
		newNode->right=createNode(level);
	}else{
		newNode->right=NULL;
	}
	return newNode;
}

void preOrder(tree *root){
	if(root ==NULL){
		return;
	}
	printf("%d\n",root->data);
	preOrder(root->left);
	preOrder(root->right);
}
void inOrder(tree *root){
	if(root ==NULL){
		return;
	}
	inOrder(root->left);
	printf("%d\n",root->data);
	inOrder(root->right);
}
void postOrder(tree *root){
	if(root ==NULL){
		return;
	}
	postOrder(root->left);
	postOrder(root->right);
	printf("%d\n",root->data);
}

void printBT(tree *root){

	char ch;

	do {
		printf("1.preOrder");
		printf("2.inOrder");
		printf("3.postOrder");

		printf("Enter your choice ");
		int choice;
		scanf("%d",&choice);

		switch(choice){

			case 1 :
				preOrder(root);
				break;
			case 2 : 
			        inOrder(root);
                                break;
			case 3 :
			        postOrder(root);
				break;
			default :
			        printf("wrong choice\n") ;
		}	
		printf("Do you want to continue?\n");
		getchar();
		scanf("%c",&ch);

	}while(ch=='y'||ch=='Y');
}



void main(){

	tree *root = (tree *)malloc(sizeof(tree));

	printf("Enter the root node\n");
	scanf("%d",&root->data);

	getchar();
	printf("Do you want to add left subtree?\n");
	char ch;
	scanf("%c",&ch);

	if(ch=='Y'||ch=='y'){

		root->left=createNode(0);
	}else{
		root->left=NULL;
	}

	printf("Do you want to add right subtree?\n");
	getchar();
	scanf("%c",&ch);

	if(ch=='Y'||ch=='y'){
		root->right=createNode(0);
	}else{
		root->right=NULL;
	}

	printBT(root);
}
