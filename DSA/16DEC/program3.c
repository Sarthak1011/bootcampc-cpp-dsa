//Implementation of Stack Using Linked List
#include <stdio.h>
#include <stdlib.h>
typedef struct node{
	int data;
	struct node *next;
}node;

node *head = NULL;
int nodecnt;
int flag = 0;

int nodeCount(){
	node *temp = head;
	int count = 0;
	while(temp != NULL){
		count++;
		temp = temp->next;
	}
	return count;
}
node* createNode(){
	node *newnode = (node*) malloc (sizeof(node));

	printf("Enter Data:\n");
	scanf("%d",&newnode->data);
	newnode->next = NULL;

	return newnode;
}
int push(){
	int count = nodeCount();
	if(count == nodecnt){
		return -1;
	}
	else{
	node *newnode = createNode();
	        if(head == NULL){
	        	head = newnode;
	        }
        	else{
			node *temp = head;
			while(temp->next != NULL){
				temp = temp->next;
			}
			temp->next = newnode;
	        }
                return 0;
	}
}
int pop(){
	int count = nodeCount();
	if(head == NULL){
		return -1;
	}
	else if(count == 0){
		return -1;
	}
	else{
		node *temp = head;
		while(temp->next->next != NULL){
			temp = temp->next;
		}
		int data = temp->next->data;
		free(temp->next);
		temp->next = NULL;
		flag++;

		return data;
	}
}
int peek(){
	if(head == NULL){
		return -1;
	}
	else{
		node *temp = head;
		while(temp->next != NULL){
			temp = temp->next;
		}
		printf("Upper Stack Data = %d\n",temp->data);
		return 0;
	}
}
void main(){
	char choice1;
	printf("Enter Node:\n");
	scanf("%d",&nodecnt);
	do{
		printf("1.push\n");
		printf("2.pop\n");
		printf("3.peek\n");

		int choice;
		printf("Enter choice:\n");
		scanf("%d",&choice);
		switch(choice){
			case 1:{
				int r1 = push();
				if(r1 == 0)
					printf("Stack Added Sucsessfully\n");
				else
					printf("Stack Overflow\n");
                               }
				break;
			case 2:{
				int r2 = pop();
				if(flag != 0){
					printf("(%d) popped\n",r2);
				}
				else{
					printf("Stack Underflow\n");
				}
				flag = 0;
			       }
				break;
		        case 3:{
				int r3 = peek();
				if(r3 == -1)
					printf("Stack Empty\n");
			       }
				break;
			default:
				printf("Wrong Choice\n");
		}
		
	getchar();
	printf("\nDo you want to continue?\n");
	scanf("%c",&choice1);
	}while(choice1 == 'y'|| choice1 == 'Y');
}
