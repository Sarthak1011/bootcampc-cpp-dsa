/*
 Program 5.
Write a program that searches all the Palindrome data elements from a
doubly linked list. And Print the position of palindrome data
Submit with a proper diagram.
Input: linked list: |12|->|121|->|30|->|252|->|35|->|151|->|70|
Output:
Palindrome found at 2
Palindrome found at 4
Palindrome found at 6
 */ 


#include<stdio.h>
#include<stdlib.h>

typedef struct demo{
	struct demo *prev;
	int data;
	struct demo *next;
}demo;

demo *head=NULL;

demo *createnode(){
	demo *node=(demo *)malloc(sizeof(demo));
	getchar();
	 node->prev=NULL;
	printf("enter the data\n");
	scanf("%d",&node->data);
	node->next=NULL;
	return node;
}
void addnode(){
	demo *node=createnode();
	if(head==NULL){
		head=node;
	}else{
		demo *temp=head;
		while(temp->next!=NULL){
			temp=temp->next;
		}
		temp->next=node;
		node->prev=temp;
	}
}
int pallindrome(){
	if(head==NULL){
		printf("linked list is empty\n");
		return -1;
	}else{
		demo *temp=head;
		int count=0;
		while(temp!=NULL){
			int num=temp->data;
			count++;
			int pal=0;
			while(num>0){
				int rem=num%10;
				 pal=pal*10+rem;
				num=num/10;
			}
			if(temp->data == pal){
				printf("pallindrome found at%d\n",count);
			}
			temp=temp->next;
		}
	}
	return 0;
}



int printLL(){
	if(head==NULL){
		printf("linked list is empty\n");
		return -1;
	}else{
		demo *temp=head;
		while(temp->next!=NULL){
			printf("|%d|->",temp->data);
			temp=temp->next;
		}

			printf("|%d|",temp->data);
	}
	return 0;
}
void main(){
	char choice;
	do {
		printf("1.addnode\n");
		printf("2.pallindrome\n");
		printf("3.printLL\n");

		printf("enter your choice\n");
		int ch;
		scanf("%d",&ch);

		switch(ch){
			case 1:
				addnode();
				break;
			case 2:
			       pallindrome();
				break;
			case 3:
				printLL();
				break;
			default :
			          printf("wrong choice\n");
		}
		 
		printf("do you want to continue\n");
		getchar();
		scanf("%c",&choice);

	}while(choice=='y'||choice=='Y');
}
             
              
 		
	
