/*
 Program 1.
Write a program that searches for the first occurrence of a particular
element from a doubly linked list.
Submit with a proper diagram.
Input linked list: |10|->|20|->|30|->|40|->|50|->|30|->|70|
Input: Enter element: 30
Output : 3
 */

#include<stdio.h>
#include<stdlib.h>

typedef struct demo {
	struct demo *prev; 
	int data;
	struct demo *next;
}demo;
demo *head=NULL;

demo *createnode(){
	demo *node=(demo *)malloc(sizeof(demo));
	node->prev=NULL;
	printf("enter the data\n");
	scanf("%d",&node->data);

	node->next=NULL;
	return node;
}
void addnode(){
	demo *node=createnode();

	if(head==NULL){
		head=node;
	}else{
		demo *temp=head;
		while(temp->next!=NULL){
			temp=temp->next;
		}
		temp->next=node;
		node->prev=temp;

	}
}
int first_occurance(){
	int num;
	printf("enter the number\n");
	scanf("%d",&num);
	if(head==NULL){
		printf("linked ist is empty\n");
		return -1;
	}else{
		int count=0;
		int flag=0;
		demo *temp=head;
		while(temp!=NULL){
			count++;
			if(num==temp->data){
				flag=1;
				break;
			}
			temp=temp->next;
		}
		if(flag==1){
			printf("%d\n",count);
		}

	}
	return 0;
}
int printLL(){
	if(head==NULL){
		printf("empty linked list\n");
		return -1;
	}else{
		demo *temp=head;
		while(temp->next!=NULL){
			printf("|%d|->",temp->data);
			temp=temp->next;
		}
			printf("|%d|\n",temp->data);
	}
	return 0;
}
void main(){
	char choice;

	do {
		printf("1.addnode\n");
		printf("2.first_occurancre\n");
		printf("3.printLL\n");

		printf("enter your choice\n");
		int ch;
		scanf("%d",&ch);

		switch(ch){
			case 1:
				addnode();
				break;
			case 2:
				first_occurance();
				break;
			case 3:
				printLL();
				break;
			default :
			         printf("wrong choice\n");
		}


	     	printf("do you to continue\n");
		getchar();
		scanf("%c",&choice);

	}while(choice=='y'||choice=='Y');
		
}

