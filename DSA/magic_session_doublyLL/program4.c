/*
 Program 4.
Write a program that adds the digits of a data element from a doubly linked
list and changes the data. (sum of data element digits)
Submit with a proper diagram.
Input linked list : |11|->|12|->|13|->|141|->|2|->|158|
Output linked list : |2|->|3|->|4|->|6|->|2|->|14|
 */ 


#include<stdio.h>
#include<stdlib.h>

typedef struct demo{
	struct demo *prev;
	int data;
	struct demo *next;
}demo;

demo *head=NULL;

demo *createnode(){
	demo *node=(demo *)malloc(sizeof(demo));
	getchar();
	 node->prev=NULL;
	printf("enter the data\n");
	scanf("%d",&node->data);
	node->next=NULL;
	return node;
}
void addnode(){
	demo *node=createnode();
	if(head==NULL){
		head=node;
	}else{
		demo *temp=head;
		while(temp->next!=NULL){
			temp=temp->next;
		}
		temp->next=node;
		node->prev=temp;
	}
}
int sumdigit(){
	if(head==NULL){
		printf("linked list is empty\n");
		return -1;
	}else{
		demo *temp=head;
		while(temp != NULL){
		
		int num=temp->data;
		int sum = 0;
		while(num > 0){
		int rem = num % 10;
		 sum = sum + rem;
		num = num / 10;
		}
		if(temp->next != NULL){
			printf("|%d|->",sum);
		}else{
			printf("|%d|",sum);
		}
		temp = temp->next;
		}
	}
	return 0;
}
int printLL(){
	if(head==NULL){
		printf("linked list is empty\n");
		return -1;
	}else{
		demo *temp=head;
		while(temp->next!=NULL){
			printf("|%d|->",temp->data);
			temp=temp->next;
		}

			printf("|%d|",temp->data);
	}
	return 0;
}
void main(){
	char choice;
	do {
		printf("1.addnode\n");
		printf("2.sumdigit\n");
		printf("3.printLL\n");

		printf("enter your choice\n");
		int ch;
		scanf("%d",&ch);

		switch(ch){
			case 1:
				addnode();
				break;
			case 2:
				sumdigit();
				break;
			case 3:
				printLL();
				break;
			default :
			          printf("wrong choice\n");
		}
		 
		printf("do you want to continue\n");
		getchar();
		scanf("%c",&choice);

	}while(choice=='y'||choice=='Y');
}
             
              
 		
	
