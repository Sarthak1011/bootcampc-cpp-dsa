/*
 Program 2.
Write a program that searches for the second last occurrence of a
particular element from a doubly linked list.
Submit with a proper diagram.
Input linked list: |10|->|20|->|30|->|40|->|30|->|30|->|70|
Input Enter element: 30
Output: 5
 */ 


#include<stdio.h>
#include<stdlib.h>

typedef struct demo{
	struct demo *prev;
	int data;
	struct demo *next;
}demo;

demo *head=NULL;

demo *createnode(){
	demo *node=(demo *)malloc(sizeof(demo));
	node->prev=NULL;
	printf("enter the data\n");
	scanf("%d",&node->data);
	node->next=NULL;
	return node;
}
void addnode(){
	demo *node=createnode();

	if(head==NULL){
		head=node;
	}else{
		demo *temp=head;
		while(temp->next!=NULL){
			temp=temp->next;
		}
		temp->next=node;
		node->prev=temp;
	}
}
int secondlast_occurance(){
	if(head==NULL){
		printf("linked list is empty\n");
		return -1;
	}else{
		int num;
		printf("enter the number\n");
		scanf("%d",&num);

	demo *temp=head;
	int count=0,previous=0,curr=0;
	while(temp!=NULL){
		count++;
		if(num==temp->data){
			previous=curr;
			curr=count;
		}
		temp=temp->next;
	}
	if(previous ==curr){
		printf("number is not present\n");
		return -1;
	}else{
	printf("second last  occurance of %d is %d\n",num,previous);
	}
	}
	return 0;
}
int printLL(){
	if(head==NULL){
		printf("linked list is empty\n");
		return -1;
	}else{
		demo *temp=head;
		while(temp->next!=NULL){
			printf("|%d|->",temp->data);
			temp=temp->next;
		}
			printf("|%d|\n",temp->data);
	}
	return 0;
}
void main(){
	char choice;
	do {
		printf("1.addnode\n");
		printf("2.secondlast_occurance\n");
		printf("printLL\n");

		printf("enter your choice\n");
		int ch;
		scanf("%d",&ch);

		switch(ch){
			case 1:
				addnode();
				break;
			case 2:
				secondlast_occurance();
				break;
			case 3:
				printLL();
				break;
			default :
			           printf("wrong choice\n");
		}
              
		printf("do you want to continue\n");
		getchar();
		scanf("%c",&choice);
	}while(choice=='y' ||choice=='Y');
}



