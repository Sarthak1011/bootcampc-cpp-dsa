/*
 Linked list structure :
struct node {
struct node *prev; // Address of prev node
char str[20]; // Data element
struct node *next; // Address of next node
};
========================================================
Program 6.
Write a program that accepts a doubly linked list from the user.
Take a number from the user and print the data of the length of that
number. Length of kanha=5
Submit with a proper diagram.
Input: linked list: |Shashi |-> | Ashish|-> |Kanha |-> | Rahul |-> | Badhe |
Input: Enter Length 5
Output:
Kanha
Rahul
Badhe
 */ 

#include<stdio.h>
#include<stdlib.h>
typedef struct demo{
	struct demo *prev;
	char str[20];
	struct demo *next;
}demo;

demo *head=NULL;

int mystrlen(char *str){
	int count=0;
	while(*str!='\0'){
		str++;
		count++;
	}
	return count;
}
demo *createnode(){
	demo *node=(demo *)malloc(sizeof(demo));
	getchar();
	node->prev=NULL;
	printf("enter the name\n");
	fgets(node->str,20,stdin);
	node->str[mystrlen(node->str)-1]='\0';
	node->next=NULL;
	return node;
}
void addnode(){
	demo *node=createnode();
	if(head==NULL){
		head=node;
	}else{
		demo *temp=head;
		while(temp->next!=NULL){
			temp=temp->next;
		}
		temp->next=node;
		node->prev=temp;
	}
}
int string_length(){
	if(head==NULL){
		printf("linked list is empty\n");
		return -1;
	}else{
		int len;
		printf("enter the string length\n");
		scanf("%d",&len);
		demo *temp=head;
		int flag=0;
		while(temp!=NULL){
			if(len==mystrlen(temp->str)){
				printf("%s\n",temp->str);
			}
			temp=temp->next;
		}
		
	}
	getchar();
	return 0;
}
int printLL(){
	if(head==NULL){
		printf("linked list is empty\n");
		return -1;
	}else{
		demo *temp=head;
		while(temp->next!=NULL){
			printf("|%s|->",temp->str);
			temp=temp->next;
		}
			printf("|%s|",temp->str);
	}
	getchar();
	return 0;
}
void main(){
	char choice;
	do {
		printf("1.addnode\n");
		printf("2.string_length\n");
		printf("3.printLL\n");

		printf("enter your choice\n");
		int ch;
		scanf("%d",&ch);

		switch(ch){
			case 1:
				addnode();
				break;
			case 2:
				string_length();
				break;
			case 3:
				printLL();
				break;
			default :
			          printf("wrong choice\n");
		}
                    
		printf("do you want to continue\n");
		scanf("%c",&choice);
	}while(choice=='y'||choice=='Y');
}
               
                        		



