//doubly circular linked list


#include<stdio.h>
#include<stdlib.h>

typedef struct demo{
	struct demo *prev;
	int data;
	struct demo *next;
}demo;

demo *head=NULL;

demo *createnode(){
	demo *node=(demo *)malloc(sizeof(demo));
	getchar();
	node->prev=NULL;
	printf("enter the data\n");
	scanf("%d",&node->data);
	node->next=NULL;
	return node;
}
void addnode(){
	demo *node=createnode();

	if(head==NULL){
		head=node;
		head->next=head;
		head->prev=head;
	}else{
		/*head->prev->next=node;
		head->prev=node;
		head->prev->next=head;
		head->prev->prev=head;*/
		head->prev->next=node;
		node->prev=head->prev;
		node->next=head;
		head->prev=node;
	}
}
void addfirst(){
demo *node=createnode();
if(head==NULL){
	head=node;
	head->next=head;
	head->prev=head;
}else{
	node->next=head;
	node->prev=head->prev;
	head->prev->next=node;
	head->prev=node;
	head=node;
}
}
void addlast(){
	addnode();
}
int countnode(){
	if(head==NULL){
		printf("linked list is empty\n");
		return -1;

	}else{
		int count=0;
		demo *temp=head;
		while(temp->next!=head){
			count++;
			temp=temp->next;
		}
		count++;
		return count;
	}
	
}

int addatpos(){
	int count=countnode();
	int pos;
	printf("enter the position\n");
	scanf("%d",&pos);
	

	if(pos<=0||pos>=count+2){
		printf("wrong position\n");
		return -1;
	}else{
		if(pos==1){
			addfirst();
		}else if(pos==count+1){
			addlast();
		}else{
			demo *temp=head;
			demo *node=createnode();
			while(pos-2){
				temp=temp->next;
				pos--;
			}
			node->next=temp->next;
			node->prev=temp;
			temp->next->prev=node;
			temp->next=node;
		}
	}
	return 0;
}
int deletefirst(){
	if(head==NULL){
		printf("linked list is empty\n");
		return -1;
	}else{
		if(head->next==head){
			free(head);
			head=NULL;
		}else{
			head=head->next;
			head->prev=head->prev->prev;
			free(head->prev->next);
			head->prev->next=head;
		}
	}
	return 0;
}
int deletelast(){
	if(head==NULL){
		printf("linked list is empty\n");
		return -1;
	}else{
		if(head->next==head){
			free(head);
			head=NULL;
		}else{
			head->prev=head->prev->prev;
			free(head->prev->next);
			head->prev->next=head;
		}
	}
	return 0;

}
int deleteatpos(){
	int count=countnode();
	int pos;
	printf("enter the position\n");
	scanf("%d",&pos);
	if(pos<=0||pos>count){
		printf("wrong position\n");
		return -1;
	}else{
		if(pos==1){
			deletefirst();
		}else if(pos==count){
			deletelast();
		}else{
			demo *temp=head;
			while(pos-2){
				temp=temp->next;
				pos--;
			}
			temp->next=temp->next->next;
			free(temp->next->prev);
			temp->next->prev=temp;
		}
	}
	return 0;
}

	
	

void printLL(){

	if(head==NULL){
		printf("linked list empty\n");
	}else{
       demo *temp=head;	
       while(temp->next!=head){
	       printf("|%d|->",temp->data);
	       temp=temp->next;
       }
       printf("|%d|\n",temp->data);
	}
}
void main(){
	char choice;
	do {
		printf("1.addnode\n");
		printf("2.addfirst\n");
		printf("3.addlast\n");
		printf("4.addatpos\n");
		printf("5.printLL\n");
		printf("6.deletefirst\n");
		printf("7.deletelast\n");
		printf("8.deleteatpos\n");

		printf("enter your choice\n");
		int ch;
		scanf("%d",&ch);

		switch(ch){
			case 1:
				addnode();
				printLL();
				break;
			case 2:
				addfirst();
				printLL();
				break;
			case 3:
				addlast();
				printLL();
				break;
			case 4:
				addatpos();
				printLL();
				break;
			case 5:
				printLL();
				break;
			case 6:
				deletefirst();
				printLL();
				break;
			case 7:
				deletelast();
				printLL();
				break;
			case 8:
				deleteatpos();
				printLL();
				break;
			default :
			         printf("wrong choice\n");
		}
                  printf("do you want to continue\n");
                  getchar();
                  scanf("%c",&choice);
	}while(choice=='y'||choice=='Y');
}	

