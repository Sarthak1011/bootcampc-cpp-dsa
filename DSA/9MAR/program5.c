/*
 Q3. Reverse in a range
Problem Description :
- Given an array A of N integers.
- Also given are two integers B and C.
- Reverse the array A in the given range [B, C]
- Return the array A after reversing in the given range.
Problem Constraints :
1 <= N <= 105
1 <= A[i] <= 109
0 <= B <= C <= N - 1
Example Input
Input 1:
A = [1, 2, 3, 4]
B = 2
C = 3
Output 1:
[1, 2, 4, 3]
Input 2:
A = [2, 5, 6]
B = 0
C = 2
Output 2:
[6, 5, 2]
Example Explanation :
Explanation 1:
We reverse the subarray [3, 4].
Explanation 2:
We reverse the entire array [2, 5, 6].
 */ 
//brute force approch



#include<stdio.h>


void main(){


	
        int size;

        printf("enter the array size\n");
        scanf("%d",&size);

        int arr[size];

        printf("enter the array elements \n");

        for(int i=0;i<size;i++){
                scanf("%d",&arr[i]);
        }
        printf("array elements are\n");

        for(int i=0;i<size;i++){
                printf("%d\t",arr[i]);
        }
	printf("\n");

	int x,y;

	printf("enter the two range numbers \n");
	scanf("%d %d",&x,&y);

	int cnt=(x+y)/2;

	for(int i=x;i<=cnt;i++){
		for(int j=y;j>=cnt;j--){
			int temp=arr[i];
			arr[i]=arr[y];
			arr[j]=temp;
			break;
		}
		y--;
	}

	for(int i=0;i<size;i++){
		printf("%d\t",arr[i]);
	}
	printf("\n");

}
		
		

	
