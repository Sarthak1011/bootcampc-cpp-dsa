/*
 Q11. Anti-Diagonal
Problem Description :
- Give an N * N square matrix A, and return an array of its anti-diagonals.
- Return a 2D integer array of size (2 * N-1) * N, representing the
anti-diagonals of input array A.
- The vacant spaces in the grid should be assigned to 0.
Example Input
Input 1:
1 2 3
4 5 6
7 8 9
Output 1:
1 0 0
2 4 0
3 5 7
6 8 0
9 0 0
Input 2:
1 2
3 4
Output 2:
1 0
2 3
4 0
Example Explanation :
For input 1:
- The first anti-diagonal of the matrix is [1 ], the rest spaces should be
filled with 0 making the row [1, 0, 0].
- The second anti-diagonal of the matrix is [2, 4 ], the rest spaces should
be filled with 0 making the row [2, 4, 0].
- The third anti-diagonal of the matrix is [3, 5, 7 ], the rest spaces
should be filled with 0 making the row [3, 5, 7].
- The fourth anti-diagonal of the matrix is [6, 8 ], the rest spaces should
be filled with 0 making the row [6, 8, 0].
- The fifth anti-diagonal of the matrix is [9 ], the rest spaces should be
filled with 0 making the row [9, 0, 0].
For input 2:
- The first anti-diagonal of the matrix is [1 ], the rest spaces should be
filled with 0 making the row [1, 0, 0].
- The second anti-diagonal of the matrix is [2, 4 ], the rest spaces should
be filled with 0 making the row [2, 4, 0].
- The third anti-diagonal of the matrix is [3, 0, 0 ], the rest spaces
should be filled with 0 making the row [3, 0, 0].
========================================================================
=========
 */ 



