/*
 Q14. Are Matrices the same?
Problem Description :
- You are given two matrices A & B of equal dimensions and you have to
check whether the two matrices are equal or not.
- Return 1 if both matrices are equal or return 0.
NOTE: Both matrices are equal if A[i][j] == B[i][j] for all i and j
in the given range.
Example Input :
Input 1:
A = [[1, 2, 3],[4, 5, 6],[7, 8, 9]]
B = [[1, 2, 3],[4, 5, 6],[7, 8, 9]]
Output 1:
1
Input 2:
A = [[1, 2, 3],[4, 5, 6],[7, 8, 9]]
B = [[1, 2, 3],[7, 8, 9],[4, 5, 6]]
Output 2:
0
Example Explanation:
Explanation 1:
- All the elements of both matrices are equal at respective positions.
Explanation 2:
- All the elements of both matrices are not equal at their respective positions.
========================================================================
=======
 */
#include<stdio.h>

void main(){

        int row,col;
        printf("enter the row and col\n");
        scanf("%d %d",&row,&col);

        int arr1[row][col];
        int arr2[row][col];

        printf("enter the array1 elements \n");
        for(int i=0;i<row;i++){
                for(int j=0;j<col;j++){
                        scanf("%d",&arr1[i][j]);
                }
        }
        printf(" array1 elements are\n");
        for(int i=0;i<row;i++){
                for(int j=0;j<col;j++){
                        printf("%d\t",arr1[i][j]);
                }
		printf("\n");
        }
        printf("enter array2 elements\n");
        for(int i=0;i<row;i++){
                for(int j=0;j<col;j++){
                        scanf("%d",&arr2[i][j]);
		}
        }
        printf("array2 elements are\n");
        for(int i=0;i<row;i++){
                for(int j=0;j<col;j++){
                        printf("%d\t",arr2[i][j]);
                }
		printf("\n");
        }
        int flag=0;
	for(int i=0;i<row;i++){
                for(int j=0;j<col;j++){
			
			if(arr1[i][j]==arr2[i][j]){
				flag=1;
			}else{
				flag=0;
			}
	         }
	}
		if(flag==1){

		printf("%d\n",flag);
                        
                }else{
			printf("%d\n",flag);
		}

                printf("\n");
        }


