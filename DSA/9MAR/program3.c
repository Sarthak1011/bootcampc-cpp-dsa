/*
 Q1. Count of elements
Problem Description:
- Given an array A of N integers.
- Count the number of elements that have at least 1 element greater than
itself.
Example Input
Input 1:
A = [3, 1, 2]
Output:
2
Explanation:
- The elements that have at least 1 element greater than itself are 1 and 2
Input 2:
A = [5, 5, 3]
Output:
1
Explanation:
- The element that has at least 1 element greater than itself is 3.

 */
// optimise apporch 2
#include<stdio.h>

void main(){

	int size;

	printf("enter the array size\n");
	scanf("%d",&size);

	int arr[size];

	printf("enter the array elements \n");

	for(int i=0;i<size;i++){
		scanf("%d",&arr[i]);
	}
	printf("array elements are\n");

	for(int i=0;i<size;i++){
		printf("%d\t",arr[i]);
	}

	int count=0;
	int max=arr[0];
	for(int i=0;i<size;i++){
		if(max<arr[i]){
		max=arr[i];
	}
	}
	for(int i=0;i<size;i++){
		if(max==arr[i]){
			count++;
		}
	}
	printf("count is %d\n",size-count);

}




	
