//LINEAR SEARCH
//last occurance second last occurance and first occurance

#include<stdio.h>
int firstocc(int arr[],int size,int key){
	for(int i=0;i<size;i++){
		if(arr[i]==key){
			return i;
		}
	}
	return -1;
}
int lastocc(int arr[],int size,int key){
	for(int i=size-1;i>=0;i--){
		if(arr[i]==key){
			return i;
		}
	}
	return -1;
}
int secondlastocc(int arr[],int size,int key){
	int seclast=-1,store=-1;
	for(int i=0;i<size;i++){
		if(arr[i]==key){
			seclast=store;
			store=i;
		}
	}
	return seclast;
}
void main(){
	int size;
	printf("enter the size\n");
	scanf("%d",&size);

	int arr[size];
	printf("enter the array elements\n");
	for(int i=0;i<size;i++){
		scanf("%d",&arr[i]);
	}
	printf("array elements are\n");
	for(int i=0;i<size;i++){
		printf("%d\t",arr[i]);
	}
	printf("\n");
	int key;
	printf("enter the key element\n");
		scanf("%d",&key);
	int first=firstocc(arr,size,key);
	if(first==-1){
		printf("not present\n");
	}else{
	printf("first occurance of %d is %d\n",key,first);
	}

	int last=lastocc(arr,size,key);
	if(last==-1){
		printf("not present\n");
	}else{

	printf("last occurance of %d is %d\n",key,last);

	}
	int secondlast=secondlastocc(arr,size,key);
	if(secondlast==-1){
		printf("not present\n");
	}else{
	printf("second last occurance of %d is %d\n",key,secondlast);
        }
}

