/*Example 1:

Input: nums = [-1,0,3,5,9,12], target = 9
Output: 4
Explanation: 9 exists in nums and its index is 4

*/

#include<stdio.h>
int flag=0;

int searchEle(int arr[],int size,int target){
	int start=0,end=size-1,mid=0;

	while(start<=end){
		mid=start+(end-start)/2;

		if(arr[mid]==target){
			return mid;
		}
		if(arr[mid]<target){
			start=mid+1;
		}
		if(arr[mid]>target){
			end=mid-1;
		}
		flag=1;
	}
	return -1;

}

void main(){
	int size;
	printf("enter the size\n");
	scanf("%d",&size);

	int arr[size];

	printf("enter the array elements\n");

	for(int i=0;i<size;i++){
		scanf("%d",&arr[i]);
	}

	int target;
	printf("enter the target element\n");
	scanf("%d",&target);

	int search=rotatedArray(arr,size,target);
	if(flag==1){
		printf("%d\n",search);
	}else{

		printf("not found\n");
	}
}


	
