//countstep
/*#include<stdio.h>
int countstep(int num){
	int count=0;
	while(num){
		if(num%2==0){
			num=num/2;
		}else{
			num=num-1;
		}
		count++;
	}
	return count;
}
void main(){
	int num;
	printf("enter the number\n");
	scanf("%d",&num);

	int count=countstep(num);
	printf("%d\n",count);
}
*/
#include<stdio.h>
int countstep(int num,int count){
	if(num==0){
		return count;
	}

		if(num%2==0){
			num=num/2;
		}else{
			num=num-1;
		}
		return countstep(num,count+1);
	}
}

void main(){
	int num;
	printf("enter the number\n");
	scanf("%d",&num);

	int count=countstep(num,0);
	printf("%d\n",count);
}

