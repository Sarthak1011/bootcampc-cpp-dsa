//reverse the number
/*

#include<stdio.h>
int reverseNum(int num){
	int rev=0;
	while(num){
		rev=rev*10+num%10;
		num=num/10;
	}
	return rev;
}

void main(){
	int num;
	printf("enter the number\n");
	scanf("%d",&num);

	int reverse=reverseNum(num);
	printf("%d\n",reverse);

}
*/
#include<stdio.h>
int reverseNum(int num,int rev){
	if(num){
		rev=rev*10+num%10;
	}else{
		return rev;
	}
	return reverseNum(num/10,rev);
}
void main(){
	int num;
	printf("enter the number\n");
	scanf("%d",&num);

	int reverse=reverseNum(num,0);
	printf("%d\n",reverse);
}
