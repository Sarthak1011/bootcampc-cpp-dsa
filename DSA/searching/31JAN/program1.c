//count the zeros from the given number
/*
#include<stdio.h>
int countzeros(int num){
	int count=0;
	while(num!=0){
		if(num%10==0){
			count++;
		}
			num=num/10;
	}
	return count;
}
void main(){
	int num;
	printf("enter the number\n");
	scanf("%d",&num);
	int count=countzeros(num);
	printf("count is=%d\n",count);
}
*/
#include<stdio.h>

int countzeros(int num,int count){
	if(num==0){
		return count;
	}
	if(num %10==0){
		count++;
	}
	return countzeros(num/10,count);
	
}

void main(){
	int num;
	printf("enter the number\n");
	scanf("%d",&num);

	if(num!=0){
	int count=countzeros(num,0);
	printf("%d\n",count);
}
else{
	printf("1");
}
}
