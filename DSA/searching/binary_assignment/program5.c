//sorted insert position



#include<stdio.h>
int flag=0;
int insertPos(int arr[],int size,int key){
	int start=0,end=size-1,mid=0,store=-1;
	if(arr[end]<key){
		store= size;
	}

	while(start<=end){
		mid=start+(end-start)/2;

		if(arr[mid]==key){
			flag=1;
			return mid;
		}
		if(arr[mid]>key){
			store=mid;//if key is less than the mid value then store it 

			end=mid-1;
		}
		if(arr[mid]<key){
			flag=2;
			start=mid+1;
		}
	}
	return store;
}
void main(){
        int size;
        printf("enter the size\n");
        scanf("%d",&size);

        int arr[size];
        printf("enter the array elements\n");
        for(int i=0;i<size;i++){
                scanf("%d",&arr[i]);
        }
        printf("array elements are\n");
        for(int i=0;i<size;i++){
                printf("%d\t",arr[i]);
        }
        printf("\n");
        int key;
        printf("enter the key element\n");
        scanf("%d",&key);
        int ret=insertPos(arr,size,key);
        if(flag==1){
                printf("%d is present int array at position %d\n",key,ret);
        }else{
                printf("inserting position of %d is %d\n",key,ret);
        }
}


