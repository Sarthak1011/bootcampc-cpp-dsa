//first occurance


#include<stdio.h>
int firstOcc(int arr[],int size,int key){
	int start=0,end=size-1,store=-1;
	while(start<=end){
		int mid=start+(end-start)/2;
		if(arr[mid]==key){
			store=mid;
		        end=mid-1;
	}
	
		if(arr[mid]>key){
			end=mid-1;
		}
		if(arr[mid]<key){
			start=mid+1;
		
		}
	}
		
	return store;
}
void main(){
        int size;
        printf("enter the size\n");
        scanf("%d",&size);

        int arr[size];
        printf("enter the array elements\n");
        for(int i=0;i<size;i++){
                scanf("%d",&arr[i]);
        }
        printf("array elements are\n");
        for(int i=0;i<size;i++){
                printf("%d\t",arr[i]);
        }
        printf("\n");
        int key;
        printf("enter the key element\n");
        scanf("%d",&key);
        int first=firstOcc(arr,size,key);
        if(first!=-1){
                printf("the floor element is the %d\n",first);
        }else{
                printf("not found\n");
        }
}
