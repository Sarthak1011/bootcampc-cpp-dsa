//find floor element using binary search

#include<stdio.h>
int flag=0;
int floorVal(int arr[],int size,int key){
	int start=0,end=size-1,mid=0,store=-1;
	while(start<=end){
		mid=(start+end)/2;
		if(arr[mid]==key){
			return arr[mid];
		}
		if(arr[mid]>key){
			end=mid-1;
		}
		if(arr[mid]<key){
			store=arr[mid];
			start=mid+1;

		}
		flag=1;
	}
	return store;
}
void main(){
        int size;
        printf("enter the size\n");
        scanf("%d",&size);

        int arr[size];
        printf("enter the array elements\n");
        for(int i=0;i<size;i++){
                scanf("%d",&arr[i]);
        }
        printf("array elements are\n");
        for(int i=0;i<size;i++){
                printf("%d\t",arr[i]);
        }
        printf("\n");
        int key;
        printf("enter the key element\n");
        scanf("%d",&key);
        int floor=floorVal(arr,size,key);
	if(flag==1){
		printf("the floor element is the %d\n",floor);
	}else{
		printf("not found\n");
	}
}

