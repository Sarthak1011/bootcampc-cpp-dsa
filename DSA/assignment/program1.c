/*Program 1. Write a program that searches all occurrences of a
particular element from a singly linear linked list.
Input linked list : |10|->|20|->|30|->|40|->|30|->|30|->|70|
Input element: 30
Output : 3*/


#include<stdio.h>
#include<stdlib.h>

typedef struct Node{
	int data;
	struct Node *next;
}node;

node *head=NULL;

node *createNode(){
	node *newNode=(node *)malloc(sizeof(node));
	printf("enter the data\n");
	scanf("%d",&newNode->data);
	newNode->next=NULL;
	return newNode;
}

void addNode(){
	node *newNode=createNode();

	if(head==NULL){
		head=newNode;
	}else{
		node *temp=head;
		while(temp->next!=NULL){
			temp=temp->next;
		}
		temp->next=newNode;
	}
}
int allOccurance(int No){
	node *temp=head;
	int count=0;
	while(temp!=NULL){
		if(temp->data==No){
			count++;
		}
		temp=temp->next;
	}
	return count;
}
int printLL(){
	if(head==NULL){
		return -1;
	}else{
		node *temp=head;
		while(temp->next!=NULL){
			printf("|%d|->",temp->data);
			temp=temp->next;
		}
			printf("|%d|\n",temp->data);
		return 0;
	}
}
void main(){
	char choice;
	do {
		printf("1.addNode\n");
		printf("2.allOccurance\n");
		printf("3.printLL\n");

		int ch;
		printf("enter your choice\n");
		scanf("%d",&ch);

		switch(ch){

			case 1:
				addNode();
				printLL();
				break;
			          
			case 2:
				{
					int  num;
					printf("enter the number of do you want to search\n");
					scanf("%d",&num);

				int ret=allOccurance(num);
					printf("the occurance of the %d is %d\n",num,ret);
				}

				break;
			          
			case 3:
				int ret =printLL();
				if(ret==-1){
					printf("linked list is empty\n");
				}
				break;
			default :
			          printf("wrong choice\n");
		}

		printf("do you want to continue\n");
		getchar();
		scanf("%c",&choice);
	}while(choice=='y'||choice=='Y');
}

			          

