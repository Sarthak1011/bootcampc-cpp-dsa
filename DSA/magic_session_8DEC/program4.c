
/*
4) WAP that add the digits of a data element from a SLL and changes the data.(sum of data elements digits).

input : linked list : |11|->|12|->|13|->|141|->|2|->|158|
output : linked list : |2|->|3|->|4|->|6|->|2|->|14|

*/


#include<stdio.h>
#include<stdlib.h>

typedef struct demo{
	int data;
	struct demo *next;
}demo;
demo *head=NULL;

demo *createnode(){
	demo *node=(demo *)malloc(sizeof(demo));
	printf("enter the data\n");
	scanf("%d",&node->data);

	node->next=NULL;
	return node;
}

void addnode(){
	demo *node=createnode();

	if(head==NULL){
		head=node;
	}else{
		demo *temp=head;
		while(temp->next!=NULL){
			temp=temp->next;
		}
		temp->next=node;
	}
}

int sum_digit(){
	if(head==NULL){
		printf("linked list is empty\n");
		return -1;
	}else{
		
		demo *temp=head;
		while(temp!=NULL){
			int num=temp->data;
			int sum=0;
			while(num>0){
				int rem=num%10;
				sum =sum+rem;
				num =num/10;
			}
			if(temp->next != NULL){
				printf("|%d|->",sum);
			}else{
				printf("|%d|",sum);
			}
			temp=temp->next;
		}
	}
	return 0;
}


int printLL(){
	if(head==NULL){
		printf("linked list is empty\n");
		return 0;
	}else{
		demo *temp=head;
		while(temp->next!=NULL){
			printf("|%d|->",temp->data);
			temp=temp->next;
		}
		printf("|%d|\n",temp->data);
	}
}
void main(){
	char choice;
	do {
		printf("1.addnode\n");
		printf("2.sum_digit\n");
		//printf("addofnode\n");
		printf("3.printLL\n");

		printf("enter your choice\n");
		int ch;
		scanf("%d",&ch);

		switch(ch){  


			case 1: 
				addnode();
				break;
			case 2: 
				sum_digit();
				break;
			case 3: 
				printLL();
				break;
					
			default :
			           printf("wrong choice\n");
		}
                                 

		printf("do you want to continue\n");
		getchar();
		scanf("%c",&choice);
	}while(choice=='y'||choice=='Y');
}




