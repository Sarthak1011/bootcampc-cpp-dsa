
/*
5) WAP that searches all the palindrome data elements from a SLL and
   Print the Position of Palindrome data.

input : linked list : |12|->|121|->|30|->|252|->|35|->|151|->|70|

output :

Palindrome found at 2
Palindrome found at 4
Palindrome found at 6
*/

#include<stdio.h>
#include<stdlib.h>

typedef struct Demo{
	int data;
	struct Demo *next;
}demo;

demo *head = NULL;

demo *createNode(){
	demo *newNode = (demo*)malloc(sizeof(demo));
	printf("Enter the data : ");
	scanf("%d",&newNode->data);

	newNode->next = NULL;
	return newNode;
}
void addNode(){
	demo *newNode = createNode();
	if(head == NULL){
		head = newNode;
	}
	else{
		demo *temp = head;
		while(temp->next != NULL){
			temp=temp->next;
		}
		temp->next = newNode;
	}
}
	
	void PallindromeNo_Position(){
	demo *temp = head;

	if(head == NULL){
		printf("LinkedList is Empty!!!\n");
		return;
	}

	int pos=0;
	while(temp!=NULL){
		pos++;
		int num = temp->data;
		int rev = 0;
		while(num>0){
		
			int rem = num%10;
			rev = rev*10 + rem;
			num = num/10;
		}
		if(rev==temp->data){
			printf("pallindrome Found at %d.\n",pos);
			
		}
		temp = temp->next;
	}
}
int printLL(){
	if(head==NULL){
		printf("linked list is empty\n");
		return -1;
	}else{
		demo *temp=head;
		while(temp->next!=NULL){
			printf("|%d|->",temp->data);
		temp=temp->next;
	}
			printf("|%d|\n",temp->data);
	return 0;
}
}
void main(){
	char choice;
	do {
		printf("1.addnode\n");
		printf("2.pallindromeNo_position\n");
		printf("3.printLL\n");

		printf("enter your choice\n");
		int ch;
		scanf("%d",&ch);

		switch(ch){

			case 1:
				addNode();
				break;
			case 2:
				PallindromeNo_Position();
				break;
			case 3:
				printLL();
				break;
			default :
			         printf("wrong choice\n");

		}


		printf("do you want to continue\n");
		getchar();
		scanf("%c",&choice);
	}while(choice=='y'||choice=='Y');
}









