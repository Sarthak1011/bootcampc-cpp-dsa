//Write a program that searches for the first occurrence of a particular
//element from a singly linear linked list.
//Input linked list: |10|->|20|->|30|->|40|->|50|->|30|->|70|
//Input: Enter element: 30
//Output : 3

#include<stdio.h>
#include<stdlib.h>

typedef struct demo{
	int data;
	struct demo *next;
}demo;

demo *head=NULL;

demo *createnode(){
	demo *node=(demo *)malloc(sizeof(demo));
	printf("enter the data\n");
	scanf("%d",&node->data);

	node->next=NULL;
	return node;
}

void addnode(){
	demo *node=createnode();
	if(head==NULL){
		head=node;
	}else{
		demo *temp=head;
		while(temp->next!=NULL){
			temp=temp->next;
		}
		temp->next=node;
	}
}
void first_occourance(){
	int num;
	printf("enter the number\n");
	scanf("%d",&num);
	int count=0;
	int flag=0;

	if(head==NULL){
		printf("linked list is empty\n");
		
	}else{
		demo *temp=head;
		while(temp!=NULL){
			count++;
			if(num==temp->data){
			     flag=1;
				break;
			}
				temp=temp->next;
		}
		if(flag==1){
			printf("index of search element is%d\n",count);
		}else{
			printf("no element is present\n");
		}

	}
}
void printLL(){
	demo *temp=head;
	
	while(temp->next!=NULL){
		printf("|%d|->",temp->data);
		temp=temp->next;
	}
	printf("|%d|\n",temp->data);
}
void main(){
	char choice;

	do {
		printf("1.addnode\n");
		printf("2.first_occurancre\n");
		printf("3.printLL\n");

		printf("enter your choice\n");
		int ch;
		scanf("%d",&ch);

		switch(ch){
			case 1:
				addnode();
				break;
			case 2:
				first_occourance();
				break;
			case 3:
				printLL();
				break;
			default :
			         printf("wrong choice\n");
		}


	     	printf("do you to continue\n");
		getchar();
		scanf("%c",&choice);

	}while(choice=='y'||choice=='Y');
}
       


