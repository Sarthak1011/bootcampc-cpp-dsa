/*
7) WAP that accepts a SLL from the user.Reverse the data elements from 
   the linked list.

input : linked list  |Shashi|->|Ashish|->|Kanha|->|Rahul|->|Badhe|

output : linked list |ishahS|->|hsihsA|->|ahnaK|->|luhaR|->|ehdaB|
*/

#include<stdio.h>
#include<stdlib.h>
#include<string.h>

typedef struct demo{
	char str[20];
	struct demo *next;
}demo;
demo *head=NULL;

char *mystrrev(char *str){
	char *temp=str;
	char x;
	while(*temp!='\0'){
		temp++;
	}
	temp--;
	while(str<temp){
		x=*str;
		*str=*temp;
		*temp=x;
		str++;
		temp--;
	}
	return str;
}
demo *createnode(){
	
	demo *node=(demo *)malloc(sizeof(demo));
	getchar();
	printf("enter the string\n");
	//fgets(node->str,20,stdin);
	//node->str[strlen(node->str)-1]='\0';
	int i=0;
	char ch;
	while((ch=getchar()) != '\n'){
	(*node).str[i]=ch;
	i++;
	}


	node->next=NULL;
	return node;
}
void addnode(){
	demo *node=createnode();

	if(head==NULL){
		head=node;
	}else{
		demo *temp=head;
		while(temp->next!=NULL){
			temp=temp->next;
		}
		temp->next=node;
	}
}
int string_rev(){
	if(head==NULL){
		printf("linked list is empty\n");
		return -1;
	}else{
		
		demo *temp=head;
		while(temp!=NULL){

		if(temp->next!=NULL){
			mystrrev(temp->str);
			printf("|%s|->",temp->str);
		}else{
			mystrrev(temp->str);
			printf("|%s|",temp->str);
		}temp=temp->next;

	}
	
	}
	getchar();
	return 0;

}
int printLL(){
	if(head==NULL){
		printf("linked list is empty\n");
		return -1;
	}else{
		demo *temp=head;
		while(temp->next!=NULL){
			printf("|%s|->",temp->str);
			temp=temp->next;

		}
			printf("|%s|\n",temp->str);
		
	}
	getchar();
	return 0;
	
}
void main(){
	char choice;
	do {
		printf("1.addnode\n");
		printf("2.string_rev\n");
		printf("3.printLL\n");
		printf("enter your choice\n");
		int ch;
		scanf("%d",&ch);
		switch(ch){

			case 1:
				addnode();
				break;
			case 2:
				string_rev();
				break;
			case 3:
				printLL();
				break;
			default :	
				printf("wrong choice\n");
		}
		printf("do you want to continue\n");
		//getchar();
		scanf("%c",&choice);

	}while(choice=='y'||choice=='Y');
	
}




