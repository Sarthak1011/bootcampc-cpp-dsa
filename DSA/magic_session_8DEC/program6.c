/*
6) WAP that accepts a SLL from the user.Take a number from the user and 
print tha data of length of that number. Length of Kanha = 5.

input : linked list: |Shashi|->|Ashish|->|Kanha|->|Rahul|->|Badhe|
input : Enter Number : 5

output :
	kanha
	Rahul
	Badhe
*/

#include<stdio.h>
#include<stdlib.h>

typedef struct demo{
	char data[20];
	struct demo *next;
}demo;

demo *head=NULL;
int mystrlen(char *str){
	int count=0;
	while(*str!='\0'){
		str++;
		count++;
	}
	return count;
}

demo *createnode(){
	demo *node=(demo *)malloc(sizeof(demo));
	getchar();
	printf("enter the string\n");
	fgets(node->data,20,stdin);
	node->data[mystrlen(node->data)-1]='\0';
	node->next=NULL;
	return node;
}
void addnode(){
	demo *node=createnode();

	if(head==NULL){
		head=node;
	}else{
		demo *temp=head;
		while(temp->next!=NULL){
			temp=temp->next;
		}
		temp->next=node;
	}
}

int string_count(){
	int num;
	printf("enter the number\n");
	scanf("%d",&num);

		if(head==NULL){
		printf("linked list is empty\n");
		return -1;
	}else{

	demo *temp=head;

	while(temp!=NULL){
		if(num==mystrlen(temp->data)){
			printf("%s\n",temp->data);
		}
		temp=temp->next;
	}
	printf("\n");
	}
	getchar();
return 0;
}
int printLL(){
	if(head==NULL){
		printf("linked list is empty\n");
		return -1;
	}else{
		demo *temp=head;
		while(temp->next!=NULL){
			printf("|%s|->",temp->data);
			temp=temp->next;
		}
			printf("|%s|\n",temp->data);
	}
	getchar();
	return 0;
	
}
void main(){
	char choice;
	do {
		printf("1.addnode\n");
		printf("2.string_count\n");
		printf("3.printLL\n");

		printf("enter ypur choice\n");
			int ch;
		scanf("%d",&ch);
		switch(ch){

			case 1:
				addnode();
				break;
			case 2:
				string_count();
				break;
			case 3:
				printLL();
				break;
		       default :
			         printf("wrong choice\n");
		}
                //getchar();
		printf("do you want to continue\n");
		scanf("%c",&choice);
	}while(choice=='y'||choice=='Y');
}





