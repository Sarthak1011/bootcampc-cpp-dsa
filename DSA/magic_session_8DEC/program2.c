/*
2) WAP that searches for the second last occurence of a particular element
from a SLL.

input : linked list : |10|->|20|->|30|->|40|->|30|->|30|->|70|.

input : Enter Element.

output : 5.
*/

#include<stdio.h>
#include<stdlib.h>

typedef struct demo {
	int data;
	struct demo *next;
}demo;

demo *head=NULL;

demo *createnode(){
	demo *node=(demo *)malloc(sizeof(demo));
	printf("enter the data\n");
	scanf("%d",&node->data);
	node->next=NULL;
	return node;
}

void addnode(){

	demo *node=createnode();
	if(head==NULL){
		head=node;
	}else{
		demo *temp=head;
		while(temp->next!=NULL){
			temp=temp->next;
		}
		temp->next=node;
	}
}
int second_last_occurance(){
	int num;
	printf("enter the number\n");
	scanf("%d",&num);
	if(head==NULL){
		printf("linked list is empty\n");
		return -1;
	}else{
		demo *temp=head;
		int prev=0,count=0,curr=0;
		while(temp!=NULL){
			count++;
			if(num==temp->data){
				prev=curr;
				curr=count;
			}
			temp=temp->next;
		}
		if(prev==curr){
			printf("not valid input\n");
		}else if(prev==0){
			printf("%d occurence only once at %d position\n",num,curr);
		}else{
				printf("second last occurance of %d is at %d position\n",num,prev);
			}
	}	
}
void printLL(){
	demo *temp = head;
	printf("Linked List : ");
	while(temp != NULL){
		if(temp->next != NULL){
			printf("|%d|->",temp->data);
		}
		else{
			printf("|%d|",temp->data);
		}
		temp = temp->next;
	}
	printf("\n");
}

void main(){
	char choice;
	do {
		printf("1.addnode\n");
		printf("2.second_last_occurance\n");
		printf("3.printLL\n");

		printf("enter your choice\n");
		int ch;
		scanf("%d",&ch);

		switch(ch){

			case 1:
				addnode();
				break;
			case 2:
				second_last_occurance();
				break;
			case 3:
				printLL();
				break;
			default :
			        printf("wrong choice\n");
		}

	printf("do you want to \n");
	getchar();
	scanf("%c",&choice);
	}while(choice=='y'||choice=='Y');
}

