/*
8) WAP that accepts a SLL from the user.Take a number from the user and only keep the
   elements that are equal in length to that number and delete other elements. And Print
   the Linked List.

   Length of Shashi = 6.

input: Linked List   |shashi|->|ashish|->|kanha|->|rahul|->|badhe|
input : 6

output = linked List  |shashi|->|ashish|
*/

#include<stdio.h>
#include<stdlib.h>

typedef struct Node{
	char str[20];
	struct Node *next;
}node;

node *head = NULL;
int mystrlen(char *str){
	int count=0;
	while(*str != '\0'){
		count++;
		str++;
	}
	return count;
}
node *createNode(){
	getchar();
	node *newNode = (node*)malloc(sizeof(node));
	printf("Enter the Name : ");
	char ch;
	int i=0;
	while((ch=getchar()) != '\n'){
		(*newNode).str[i] = ch;
		i++;
	}
	newNode->next = NULL;
	return newNode;
}
void addNode(){
	node *newNode = createNode();
	if(head == NULL){
		head = newNode;
	}
	else{
		node *temp = head;
		while(temp->next != NULL){
			temp=temp->next;
		}
		temp->next = newNode;
	}
}
void printUserLengthString(){
	node *temp = head;
	if(head == NULL){
		printf("Linked List is Empty!!!.\n");
	}
	else{

		int len;
		printf("Enter the Length of String : ");
		scanf("%d",&len);

		while(temp!=NULL){

			if(len == mystrlen(temp->str)){
				if(temp->next != NULL){
					printf("|%s|->",temp->str);
				}
				else{
					printf("|%s|",temp->str);
				}
			}

			else{
				free(temp->str);
			}	
			temp = temp->next;
		}
	}
		printf("\n");
		getchar();
}	
void printLL(){
	node *temp = head;
	while(temp != NULL){
		if(temp->next != NULL){
			printf("|%s|->",temp->str);
		}
		else{
			printf("|%s|",temp->str);
		}
		temp = temp->next;
	}
	printf("\n");
	getchar();
}
void main(){
	char choice;
	do {
		printf("1.addnode\n");
		printf("2.printUserLengthString\n");
		printf("3.printLL\n");

		printf("enter your choice\n");
		int ch;
		scanf("%d",&ch);

		switch(ch){
			case 1:
				addNode();
				break;
			case 2:
				printUserLengthString();
				break;
			case 3:
				printLL();
				break;
			default :
			          printf("wrong choice\n");
 		}

		printf("do you want to continue\n");
		scanf("%c",&choice);
	}while(choice=='y'||choice=='Y');
}



