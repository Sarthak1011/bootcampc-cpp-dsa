#include<stdio.h>
#include<stdlib.h>

typedef struct Node{
	int data;
	struct Node *next;
}n;

void main(){

	n *head=NULL;//starting head pointer is null
	//Frist node
	n *newNode=(n *)malloc(sizeof(n));
	newNode->data=10;
	newNode->next=NULL;

	head=newNode;

	//second node
	newNode=(n *)malloc(sizeof(n));
	newNode->data=20;
	newNode->next=NULL;

	//connect first node second
	head->next=newNode;

	//third node
	newNode=(n *)malloc (sizeof(n));
	newNode->data=30;
	newNode->next=NULL;

	//connect second node to third
	head->next->next=newNode;


	n *temp=head;// take temporary pointer 

	while(temp !=NULL){
		printf("%d ",temp->data);
		temp=temp->next;
	}
	printf("\n");

}

