#include<stdio.h>
#include<stdlib.h>
typedef struct node{
	int data;
	struct node *next;
}node;

void main(){
	//node1
	node *newnode=(node *)malloc(sizeof(node));

	//head pointer
	node *head=newnode;
	newnode->data=10;
	newnode->next=NULL;
	//node2
	newnode=(node *)malloc(sizeof(node));
	newnode->data=20;
	//next of first node
	head->next=newnode;

	//node 3
	newnode=(node *)malloc(sizeof(node));
	newnode->data=30;
	//next of second node
	head->next->next=newnode;
	//next of last node
	head->next->next->next=NULL;
	//temp pointer
	node *temp=head;
	//accessing data link list
	while(temp !=NULL){
		if(temp->next !=NULL){
			printf("|%d|->",temp->data);
		}else{
			printf("|%d|",temp->data);
		
		}
		temp=temp->next;
	}
		printf("\n");
	
}
