//implementing two stack using array


#include<stdio.h>
#include<stdbool.h>


int size=0;
int top1=0;
int top2=0;
int flag=0;

 bool isfull(){
	 if(top2-top1==1){
	return true;
   }else{
	return false;
     }
}
int push1(int arr[]){
	if(isfull()){
		return -1;
	}else{
		top1++;
		printf("enter the data\n");
		scanf("%d",&arr[top1]);
	}
	return 0;
}
int push2(int arr[]){
	if(isfull()){
		return -1;
	}else{
		top2--;
		printf("enter the data\n");
		scanf("%d",&arr[top2]);
	}
	return 0;
}
bool isempty1(){
	if(top1==-1){
		return true;
	}else{
		return false;
	}
}
bool isempty2(){
	if(top2==size){
		return true;
	}else{
		return false;
	}
}

int pop1(int arr[]){
	if(isempty1()){
		flag=0;
		return -1;
	}else{
		flag=1;
		int data=arr[top1];
		top1--;
		return data;
	}
}
int pop2(int arr[]){
	if(isempty2()){
		flag=0;
		return -1;
	}else{
		flag=1;
		int data=arr[top2];
		top2++;
		return data;
	}
}
int peek1(int arr[]){
	if(isempty1()){
		flag=0;
		return -1;
	}else{
		flag=1;
		int data=arr[top1];
		return data;
	}
}
int peek2(int arr[]){
	if(isempty2()){
		flag=0;
		return -1;
	}else{
		flag=1;
		int data=arr[top2];
		return data;
	}
}


void main(){
	printf("enter the size of stack\n");
	scanf("%d",&size);

	int arr[size];
	top1=-1;
	top2=size;

	char choice;
	do {
		printf("1.push1\n");
		printf("2.push2\n");
		printf("3.pop1\n");
		printf("4.pop2\n");
		printf("5.peek1\n");
		printf("6.peek2\n");

		printf("enter your choice\n");
		int ch;
		scanf("%d",&ch);

		switch(ch){

			case 1:{
				int ret=push1(arr);
				if(ret==-1){
					printf("stack overflow\n");
				}
			       }
		
				break;
			case 2:
				{
				int ret=push2(arr);
				if(ret==-1){
					printf("stack overflow\n");
				}
			       }
		         	break;	
			case 3:{
				int ret=pop1(arr);
				if(flag==0){
					printf("stack underflow \n");
				}else{
					printf("%d is popped from stack 1\n",ret);
				}
			       }
				break;
			case 4:{
			        int ret=pop2(arr);
				if(flag==0){
					printf("stack underflow \n");
				}else{
					printf("%d is popped from stack 2\n",ret);
				}
			       }
		         	break;	
			case 5:{
				int ret=peek1(arr);
				if(flag==0){
					printf("stack underflow\n");
				}else{
					printf("%d is printed from stack 1\n",ret);
				}
			       }
		
				break;
			case 6:
				{ 
				int ret=peek2(arr);
				if(flag==0){
					printf("stack underflow\n");
				}else{
					printf("%d is printed from stack 2\n",ret);
				}
			       }
		         	break;	

			default :
			       printf("wrong choice\n");
		}                 


		printf("do you want to continue\n");
		getchar();
		scanf("%c",&choice);
	}while(choice=='Y'||choice=='y');
}

		
