#include<stdio.h>
#include<stdlib.h>
#include<string.h>

typedef struct player{
	char name[20];
	int jerNo;
	float avg;
	struct player *next;
}p;

void main(){

	p *p1=(p *)malloc(sizeof(p));
	p *p2=(p *)malloc(sizeof(p));
	p *p3=(p *)malloc(sizeof(p));

	strcpy(p1->name,"surya");
	p1->jerNo=63;
	p1->avg=63.33;
	p1->next=p2;

	printf("%s\n",p1->name);
	printf("%d\n",p1->jerNo);
	printf("%f\n",p1->avg);
	printf("\n");

	strcpy(p1->next->name,"hardik");
	p1->next->jerNo=33;
	p1->next->avg=33.33;
	p1->next->next=p3;
	
	printf("%s\n",p1->next->name);
	printf("%d\n",p1->next->jerNo);
	printf("%f\n",p1->next->avg);
	printf("\n");
	
	strcpy(p1->next->next->name,"rohit");
	p1->next->next->jerNo=45;
	p1->next->next->avg=45.33;
	p1->next->next->next=NULL;

	printf("%s\n",p1->next->next->name);
	printf("%d\n",p1->next->next->jerNo);
	printf("%f\n",p1->next->next->avg);
	printf("\n");
}

