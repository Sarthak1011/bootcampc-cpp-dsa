#include<stdio.h>
#include<stdlib.h>
typedef struct student{
	int id;
	float ht;
	struct student *next;
}stud;

void main(){
	stud *head=NULL;
	//first node
	stud *node=(stud *)malloc(sizeof(stud));
	node->id=1;
	node->ht=4.5;
	node->next=NULL;
	
	head=node;
	//second node
        node=(stud *)malloc(sizeof(stud));
	node->id=2;
	node->ht=5.5;
	node->next=NULL;

	//connect second node to first
	head->next=node;

	//third node
	node=(stud *)malloc(sizeof(stud));
	node->id=3;
	node->ht=6.5;
	node->next=NULL;

	//connect third node to second
	head->next->next=node;

	stud *temp=head;

	while(temp !=NULL){
		printf("|%d|",temp->id);
		printf("|%f|->",temp->ht);
		temp=temp->next;
	}
}

