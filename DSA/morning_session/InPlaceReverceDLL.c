//doubly linked list in place reverce


#include<stdio.h>
#include<stdlib.h>

typedef struct demo {
	struct demo *prev;
	int data;
	struct demo *next;
}demo;


demo *head=NULL;

demo *createnode(){
	demo *node=(demo *)malloc(sizeof(demo));
	node->prev=NULL;
	printf("enter the data\n");
	scanf("%d",&node->data);
	node->next=NULL;
	return node;
}
void addnode(){
	demo *node=createnode();

	if(head==NULL){
		head=node;
	}else{
		demo *temp=head;
		while(temp->next!=NULL){
			temp=temp->next;
			
		}
		temp->next=node;
		node->prev=temp;
	}
}
int printLL(){
	if(head==NULL){
		printf("linked list is empty\n");
		return -1;
	}else{
		demo *temp=head;
		while(temp->next!=NULL){
			printf("|%d|",temp->data);
			temp =temp->next;
		}
			printf("|%d|\n",temp->data);
	}
	return -1;
}
int InPlaceReverceDLL(){
	if(head==NULL){
		printf("linked list is empty\n");
		return -1;
	}else{
		demo *temp=NULL;
		while(head!=NULL){
			head->prev=head->next;
			head->next=temp;
			temp=head;
			head=head->prev;
		}
		head=temp;
	}
	return 0;
}
void main(){

	printf("enter the node count\n");
	int nodecount;
	scanf("%d",&nodecount);

	for(int i=0;i<nodecount;i++){
		addnode();
		printLL();
	}
	InPlaceReverceDLL();
	printLL();
}

