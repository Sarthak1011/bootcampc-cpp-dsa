#include<stdio.h>
#include<stdlib.h>

typedef struct demo{
	int data;
	struct demo *next;
}demo;

demo *head=NULL;
demo *createnode(){
	demo *node=(demo *)malloc (sizeof(demo));
	printf("enter the data\n");
	scanf("%d",&node->data);

	node->next=NULL;
	return node;

}

void addnode(){
	demo *node=createnode();

	if(head==NULL){
		head=node;
	}else{
		demo *temp=head;
		while(temp->next!=NULL){
			temp=temp->next;
		}
		temp->next=node;
	}
}

int printLL(){
	if(head==NULL){
		printf("linked list is empty\n");
		return -1;
	}else{
		demo *temp=head;
		while(temp->next!=NULL){
			printf("|%d|->",temp->data);
			temp=temp->next;
		}
			printf("|%d|\n",temp->data);
	}
	return 0;
}

int InPlaceReverce(){
	if(head==NULL){
		printf("linked list is empty\n");
		return -1;
	}else{
		demo *temp1=NULL,*temp2=NULL;
	while(head!=NULL){
		temp2=head->next;
		head->next=temp1;
		temp1=head;
		head=temp2;
	}
	head=temp1;
	}
	return 0;
}

void main(){
	int nodecount;
	printf("enter the node count\n");
	scanf("%d",&nodecount);

	for(int i=1;i<=nodecount;i++){
		addnode();
		printLL();
	}
	InPlaceReverce();
	printLL();
}


