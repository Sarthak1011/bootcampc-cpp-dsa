//singly circular linked list

#include<stdio.h>
#include<stdlib.h>

typedef struct demo{
	int data;
	struct demo *next;
}demo;

demo *head=NULL;

demo *createnode(){
	demo *node=(demo *)malloc(sizeof(demo));
	getchar();
	printf("enter the data\n");
	scanf("%d",&node->data);
	node->next=NULL;
	return node;
}
void addnode(){
	demo *node=createnode();
	if(head==NULL){
		head=node;
		head->next=head;
	}else{
		demo *temp=head;
		while(temp->next!=head){
			temp=temp->next;
		}
		temp->next=node;
		node->next=head;
	}
}
void addfirst(){
	demo *node=createnode();

	if(head==NULL){
		head=node;
		head->next=head;
	}else{
		demo *temp=head;
		while(temp->next!=head){
			temp=temp->next;
		}
		node->next=head;
		head=node;
		temp->next=head;
	}
}
void addlast(){
	addnode();
}
int countnode(){
	demo *temp=head;
	int count=0;
	while(temp->next!=head){
		temp=temp->next;
		count++;
	}
	count++;
	return count;
}
int addatpos(){
	int pos;
	printf("enter the position\n");
	scanf("%d",&pos);
	int count=countnode();

	if(pos<=0 || pos>count+1){
		printf("wrong position\n");
		return -1;
	}else{
		if(pos==1){
			addfirst();
		}else if(pos==count+1){
			addlast();
		}else{
			demo *temp=head;
			demo *node=createnode();
			while(pos-2){
				temp=temp->next;
				pos--;
			}
			node->next=temp->next;
			temp->next=node;
		}

	}
	return 0;
}
int deletefirst(){

	if(head==NULL){
		printf("linked list is empty\n");
		return -1;
	}else{
		if(head->next==head){
			free(head);
			head=NULL;
		}else{
			demo *temp=head;
			while(temp->next!=head){
				temp=temp->next;
			}
			head=head->next;
			free(temp->next);
			temp->next=head;
		}
		
	}
	return 0;
}
int deletelast(){
	if(head==NULL){
		printf("linked list is empty\n");
		return -1;
	}else{
		if(head->next==head){
			free(head);
			head=NULL;
		}else{
			demo *temp=head;
			while(temp->next->next!=head){
				temp=temp->next;
			}
			free(temp->next);
			temp->next=head;
		}
	}
	return 0;
}
int deleteatpos(){
	int pos;
	printf("enter the position\n");
	scanf("%d",&pos);
	int count=countnode();

	if(pos<=0||pos>count){
		printf("linked list is empty\n");
		return -1;
	}else{
		if(pos==1){
			deletefirst();
		}else if(pos==count){
			deletelast();
		}else{
			demo *temp1=head,*temp2=head;
		       while(pos-2){
		       temp2=temp2->next;
		       pos--;
		       }
		       temp1=temp2->next;
		       temp2->next=temp1->next;
		       free(temp1);
		}
	}
	return 0;
}
int printLL(){
	if(head==NULL){
		printf("linked list is empty\n");
		return -1;
	}else{
		demo *temp=head;
		while(temp->next!=head){
			printf("|%d|->",temp->data);
			temp=temp->next;
		}
			printf("|%d|",temp->data);
	}
	return 0;
}


void main(){
	char choice;
	do {
		printf("1.addnode\n");
		printf("2.addfirst\n");
		printf("3.addlast\n");
		printf("4.addatps\n");
		printf("5.printLL\n");
		printf("6.addfirst\n");
		printf("7.addlast\n");
		printf("8.addatps\n");

		printf("enter your choice\n");
		int ch;
		scanf("%d",&ch);

		switch(ch){

			case 1:
				addnode();
				break;
			case 2:
				addfirst();
				break;
			case 3:
				addlast();
				break;
			case 4:
				addatpos();
				break;
			case 5:
				printLL();
				break;
			case 6:
				deletefirst();
				break;
			case 7:
				deletelast();
				break;
			case 8:
				deleteatpos();
				break;
			default :
			          printf("wrong choice\n");
		}


		printf("do you to continue\n");
		getchar();
		scanf("%c",&choice);
	}while(choice=='y'||choice=='Y');
}
                          
                         		
				





				


