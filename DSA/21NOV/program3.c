//Dailyflash #6
//Write a realtime example of structure access to the data using a pointer.(Draw diagram).

#include<stdio.h>
#include<string.h>

struct garden{
	char name[20];
	int count;
	float height;
	struct garden *next;
};

void main(){
	struct garden obj1,obj2,obj3;

	struct garden *head=&obj1;

	strcpy(head->name,"mango");
	head->count=5;
	head->height=3.4;
	head->next=&obj2;

	printf("Name of tree is %s\n",head->name);
	printf("total trees are %d\n",head->count);
	printf("height is %f\n\n",head->height);

	strcpy(head->next->name,"coconut");
	head->next->count=5;
	head->next->height=4.5;
	head->next->next=&obj3;

	printf("Name of tree is %s\n",head->next->name);
	printf("total trees are %d\n",head->next->count);
	printf("height is %f\n\n",head->next->height);

	strcpy(head->next->next->name,"guava");
	head->next->next->count=4;
	head->next->next->height=3.7;
	head->next->next->next=NULL;

	printf("Name of tree is %s\n",head->next->next->name);
	printf("total trees are %d\n",head->next->next->count);
	printf("height is %f\n",head->next->next->height);

}
