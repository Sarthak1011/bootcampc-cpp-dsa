//
#include<stdio.h>
#include<string.h>

struct batsman{
	int jerNo;
	char name[20];
	float avg;
	struct batsman *next;
};
void main(){
	struct batsman obj1,obj2,obj3;
	struct batsman *head=&obj1;

	head->jerNo=63;
	strcpy(head->name,"Surykumar Yadav");
	head->avg=63.22;
	head->next=&obj2;

	printf("jercy number:%d\n",head->jerNo);
	printf("1st batsman name:%s\n",head->name);
	printf("average:%f\n",head->avg);
	printf("\n");

	head->next->jerNo=45;
	strcpy(head->next->name,"Rohit Sharma");
	head->next->avg=45.33;
	head->next->next=&obj3;

	printf("jercy number:%d\n",head->next->jerNo);
	printf("2nd batsman name:%s\n",head->next->name);
	printf("average:%f\n",head->next->avg);
	printf("\n");
	
	head->next->next->jerNo=33;
	strcpy(head->next->next->name,"Hardik Pandya");
	head->next->next->avg=33.44;
	head->next->next->next=NULL;

	printf("jercy number:%d\n",head->next->next->jerNo);
	printf("3rd batsman name:%s\n",head->next->next->name);
	printf("average:%f\n",head->next->next->avg);
	printf("\n");
	
}
