//self referential structure


#include<stdio.h>
#include<stdlib.h>
#include<string.h>
typedef struct Employee{
	char name[20];
	int empid;
	float sal;
	struct Employee *next;
}emp;

void main(){
	emp obj1,obj2,obj3;

	emp *head=&obj1;
	strcpy(obj1.name,"sarthak");
	obj1.empid=1;
	obj1.sal=15.000;
         obj1.next=&obj2;

	printf("%s\n",head->name);
	printf("%d\n",head->empid);
	printf("%f\n",head->sal);

	strcpy(obj2.name,"abhishek");
	obj2.empid=2;
	obj2.sal=17.000;
	obj2.next=&obj3;

	printf("%s\n",obj1.next->name);
	printf("%d\n",obj1.next->empid);
	printf("%f\n",obj1.next->sal);


	strcpy(obj3.name,"amar");
	obj3.empid=3;
	obj3.sal=18.000;
	obj3.next=NULL;

	printf("%s\n",obj2.next->name);
	printf("%d\n",obj2.next->empid);
	printf("%f\n",obj2.next->sal);
}
