// character array using recursion
//

#include<stdio.h>
int characterArray(char arr[],int size,char ch){
	if(ch==arr[size]){
		return size;
	}
	
	return characterArray(arr,size-1,ch);
}
void main(){
	int size;
	printf("enter the array size\n");
	scanf("%d",&size);

	char carr[size];

	printf("enter the array elements\n");
	for(int i=0;i<size;i++){
		scanf(" %c",&carr[i]);
	}
	printf("array elements are\n");
	for(int i=0;i<size;i++){
		printf("%d = %c\n",i,carr[i]);
	}
	char search;
	getchar();
	printf("enter the charcter do you want to search\n");
	scanf("%c",&search);
	int ret =characterArray(carr,size-1,search);

	printf("the index of the '%c' charcter int array is %d \n",search,ret);
}

