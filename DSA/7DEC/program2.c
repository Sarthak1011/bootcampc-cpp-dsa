// proper code of singly link list of deletefirst ,deletelast, deleteatpos

#include<stdio.h>
#include<stdlib.h>
#include<string.h>

typedef struct player {
	char pName[20];
	int jerNo;
	struct player *next;
}player;

player *head=NULL;

void addNode(){
	player *node=(player *)malloc (sizeof(player));
	getchar();
	printf("enter the player name\n");
	fgets(node->pName,20,stdin);
	node->pName[strlen(node->pName)-1]='\0';
	printf("enter the jercy number\n");
	scanf("%d",&node->jerNo);
	node->next=NULL;

	if(head==NULL){
		head=node;
	}else{
		player *temp=head;
		while(temp->next!=NULL){
			temp=temp->next;
		}
		temp->next=node;
	}
}
int delFirst(){

	if(head==NULL){
		printf("There is no node to delete\n");
		return -1;
	}else{
		if(head->next==NULL){
			free(head);
			head=NULL;

		}else{
			player *temp=head;
			while(temp->next!=NULL){
				temp=temp->next;
			}
			head=temp->next;
			free(temp);
		}
	}
	return 0;
}
int delLast(){
	if(head==NULL){
		printf("there is no node to delete\n");
		return -1;
	}else{
		if(head->next==NULL){
			free(head);
			head=NULL;
		}else{
			player *temp=head;
			while(temp->next->next!=NULL){
				temp=temp->next;
			}
			free(temp->next);
			temp->next=NULL;
		}
	}
	return 0;
}
int countnode(){
	player *temp=head;
	int count=0;
	while(temp!=NULL){
		temp=temp->next;
		count++;
	}
	return count;
}

int delAtPos(){
	int pos;
	printf("enter the position\n");
	scanf("%d",&pos);

	int count=countnode();
	if(pos<=0 || pos>count){
		printf("there is no node to delete\n");
	return -1;
	}else{
		if(pos==1){
			delFirst();
		}else if(pos==count){
			delLast();
		}else{
			player *temp1=head,*temp2=head;
			while(pos-2){
				temp1=temp1->next;
				temp2=temp2->next;
				pos--;
			}
			temp1=temp1->next;
			temp2->next=temp2->next->next;
			free(temp1);
		}
	}
	return 0;
}
int printLL(){
		if(head==NULL){
			printf("there is no node to delete\n");
			return -1;
		}
		player *temp=head;
		while(temp->next!=NULL){
			printf("|%s->",temp->pName);
			printf("%d|->",temp->jerNo);
			temp=temp->next;
		}
			printf("|%s->",temp->pName);
			printf("%d|\n",temp->jerNo);
		
	
	return 0;
}

void main(){
	char choice;
	do {
		printf("1.addNode\n");
		printf("2.delFirst\n");
		printf("3.delLast\n");
		printf("4.delAtPos\n");
		printf("5.printLL\n");

		printf("enter your choice\n");
		int ch;
		scanf("%d",&ch);

		switch(ch){
			case 1:
				addNode();
				break;
			case 2:
				delFirst();
				break;
			case 3:
				delLast();
				break;
			case 4:
				delAtPos();
				break;
			case 5:
				printLL();
				break;
			default :
			         printf("wronng choice\n");
	}
		printf("do you want to continue\n");
		getchar();
		scanf("%c",&choice);
	}while(choice=='y'||choice=='Y');
}







