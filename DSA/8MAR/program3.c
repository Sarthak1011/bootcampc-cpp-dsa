//subarray apporach 2 o(n^2)
//using prefix sum
#include<stdio.h>

void main(){
	int size;
	printf("enter the array size\n");
	scanf("%d",&size);

	int arr[size];

	printf("enter the array elements\n");

	for(int i=0;i<size;i++){
		scanf("%d",&arr[i]);
	}

	printf("array elements are\n");

	for(int i=0;i<size;i++){
		printf("%d\t",arr[i]);
	}
	printf("\n");


	int psum[size];
	psum[0]=arr[0];
	for(int i=1;i<size;i++){
		psum[i]=psum[i-1]+arr[i];
	}

	for(int i=0;i<size;i++){
		for(int j=i;j<size;j++){
		int sum=0;

		if(i==0){
			sum=psum[j];
		}else{
			sum=psum[j]-psum[i-1];
		}
		printf("%d\n",sum);
	}
		//printf("\n");

}
}

	
