//swapping
#include<stdio.h>
#include<stdlib.h>

typedef struct demo{
	struct demo *prev;
	int data;
	struct demo *next;
}demo;
demo *head=NULL;
demo *createnode(){
	demo *node=(demo *)malloc(sizeof(demo));
	getchar();
	node->prev=NULL;
	printf("enter the data\n");
	scanf("%d",&node->data);
	node->next=NULL;
	return node;
}
void addnode(){
	demo *node=createnode();

	if(head==NULL){
		head=node;
	}else{
		demo *temp=head;
		while(temp->next!=NULL){
			temp=temp->next;
		}
		temp->next=node;
		node->prev=temp;
	}
}
int countnode(){
	int count=0;
	demo *temp=head;
	while(temp!=NULL){
		count++;
		temp=temp->next;
	}
	return count;
}
int swap_data(){
	if(head==NULL){
		printf("linked list is empty\n");
		return -1;
	}else{

	int count=countnode();
	demo *temp1=head,*temp2=head;
	while(temp2->next!=NULL){
		temp2=temp2->next;
	}
	int cnt=count/2;

            	while(cnt){
 	        	int x=temp1->data;
	        	temp1->data=temp2->data;
		        temp2->data=x;

	        	temp1=temp1->next;
	        	temp2=temp2->prev;
			cnt--;
        	}
	}
           return 0;
}
int printLL(){
	if(head==NULL){
		printf("linked list is empty\n");
		return -1;
	}else{
		demo *temp=head;
		while(temp->next!=NULL){
			printf("|%d|->",temp->data);
			temp=temp->next;
		}
			printf("|%d|\n",temp->data);
	}
	return 0;
}
void main(){
	char choice;
	do {
		printf("1.addnode\n");
		printf("2.swap_data\n");
		printf("3.printLL\n");

		printf("enter your choice\n");
		int ch;
		scanf("%d",&ch);

		switch(ch){
			case 1:
			       addnode();
			       break;
			case 2:
			      swap_data();
			       break;
			case 3:
			       printLL();
			       break;
		      default :
			      printf("wrong choice\n");
		} 


		printf("do you want to continue\n");
		getchar();
		scanf("%c",&choice);
	}while(choice=='y'||choice=='Y');
}

	


