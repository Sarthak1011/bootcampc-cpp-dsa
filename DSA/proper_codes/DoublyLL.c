#include<stdio.h>
#include<stdlib.h>
 typedef struct Node{
	 struct Node *prev;
	 int data;
	 struct Node *next;
 }Node;
Node *head=NULL;

Node *createNode(){
	Node *newNode=(Node *)malloc(sizeof(Node));
	newNode->prev=NULL;
	printf("enter the data\n");
	scanf("%d",&newNode->data);
	newNode->next=NULL;
	return newNode;
}
void addNode(){
	Node *newNode=createNode();

	if(head==NULL){
		head=newNode;
	}else{
		Node *temp=head;
		while(temp->next!=NULL){
			temp=temp->next;
		}
		temp->next=newNode;
		newNode->prev=temp;
	}

}
void addFirst(){
	Node *newNode=createNode();

	if(head==NULL){
		head=newNode;
	}else{
		newNode->next=head;
		head->prev=newNode;
		head=newNode;
	}
}
void addLast(){
	addNode();
}
int countNode(){
	Node *temp=head;
	int count=0;
	while(temp!=NULL){
		temp=temp->next;
		count++;
	}
	return count;
}
int addAtPos(int pos){

	if(pos<=0 || pos>=countNode()+2){
		return -1;
	}else{
		if(pos==1){
			addFirst();
		}else if(pos==countNode()+1){
			addLast();
		}else{
			Node *newNode=createNode();
			Node *temp=head;
			while(pos-2){
				temp=temp->next;
				pos--;
			}
			newNode->next=temp->next;
			newNode->prev=temp;
			temp->next->prev=newNode;
			temp->next=newNode;
		}
		return 0;
	}
}
int deleteFirst(){

	if(head==NULL){
		return -1;
	}else{
		if(head->next==NULL){
			free(head);
			head=NULL;
		}else{
			head=head->next;
			free(head->prev);
			head->prev=NULL;
		}
	}
	return 0;
}

int deleteLast(){
	if(head==NULL){
		return -1;
	}else{
		if(head->next==NULL){
			free(head);
			head=NULL;
		}else{
			Node *temp=head;
			while(temp->next->next!=NULL){
				temp=temp->next;
			}
			free(temp->next);
			temp->next=NULL;
		}
		return 0;
	}
}

int deleteAtPos(int pos){
	int count=countNode();

	if(pos<=0||pos>count){
		return -1;
	}else{
		if(pos==1){
			deleteFirst();
		}else if(pos==count){
			deleteLast();
		}else{
			Node *temp=head;
			while(pos-2){
				temp=temp->next;
				pos--;
			}
			temp->next=temp->next->next;
			free(temp->next->prev);
			temp->next->prev=temp;
		}
		return 0;
		}
}
int printLL(){
	if(head==NULL){
		return -1;
	}else{
		Node *temp=head;
		while(temp->next!=NULL){
			printf("|%d|->",temp->data);
			temp=temp->next;
		}
			printf("|%d|\n",temp->data);
			return 0;
	}
	
}




void main(){


        char choice;

        do {
                printf("!....*Your choice*....!\n");
                printf("1.addNode\n");
                printf("2.addFirst\n");
                printf("3.addLast\n");
                printf("4.addAtPos\n");
                printf("5.deletsFirst\n");
                printf("6.deleteLast\n");
                printf("7.deleteAtPos\n");
                printf("8.printLL\n");

                int ch;
                printf("enter your choice\n");
                scanf("%d",&ch);

                switch(ch){

                        case 1:
                                addNode();
                                printLL();
                                break;
                        case 2:
                                addFirst();
                                printLL();
                                break;
                        case 3:
                                addLast();
                                printLL();
                                break;
                        case 4:{
                               int pos;
                               printf("enter the postion of node\n");
                               scanf("%d",&pos);
                               int ret=addAtPos(pos);
                               if(ret==-1){
                                       printf("you entered wrong position\n");
                               }
                               }
                                printLL();
                                break;
                        case 5:{
                                int ret=deleteFirst();
                                if(ret == -1){
                                        printf("There is no node to delete\n");
                                }
                               }
                                printLL();
                                break;
                        case 6:{
                                int ret=deleteLast();
                                if(ret==-1){
                                        printf("There is no to delete\n");
                                }
                               }
                                printLL();
                                break;
                        case 7:{
                                       int pos;
                                       printf("enter the postion\n");
                                       scanf("%d",&pos);

                                int ret=deleteAtPos(pos);
                                if(ret==-1){
                                        printf("There is no node to delete\n");
                                }
                               }
                                printLL();
                                break;
                        case 8:{
                                int ret=printLL();
                                if(ret==-1){
                                        printf("linked list is empty\n");
                                }
                               }

                                break;
                        default :
                                     printf("wrong choice\n");
                }


                printf("do you want to continue\n");
                getchar();
                scanf("%c",&choice);
        }while(choice=='Y'||choice=='y');
}



