// operations on pointer




#include<stdio.h>
void main(){

	int x=10;
	char ch='A';

	int *ptr1=&x;
	char *ptr2=&ch;

	printf("%p\n",ptr1);//ox100
	printf("%p\n",ptr2);//ox200

	printf("%d\n",*ptr1);//10
	printf("%c\n",*ptr2);//A
}
