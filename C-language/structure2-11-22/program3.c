#include<stdio.h>
struct picnic {
	char location[20];
	int count;
	float distance;
};
void mystrcpy(char *dest,char *src){
	while(*src!='\0'){
		*dest=*src;
		dest++;
		src++;
	}
	*dest='\0';
}
void main(){
	struct picnic obj1;
	mystrcpy(obj1.location,"kashmir");
	obj1.count=2;
	obj1.distance=1200.50;

	printf("%s\n",obj1.location);
	printf("%d\n",obj1.count);
	printf("%f\n",obj1.distance);
}

