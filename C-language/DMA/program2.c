// Malloc


#include<stdio.h>
//#include<stdlib.h>
void * malloc(size_t size);// prototype of malloc
void fun(){
	int x=10;
//	int *ptr=malloc(sizeof(int));//implicit typecasting
	int *ptr=(int *)malloc(sizeof(int));//explicit typecasting
	*ptr=50;

	printf("%d\n",*ptr);
}
void main(){
	fun();
}
