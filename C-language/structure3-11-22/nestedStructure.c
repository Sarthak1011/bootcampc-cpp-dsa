// nested structure

#include<stdio.h>
#include<string.h>

struct movieInfo{
	char actor[20];
	float imdrate;
};
struct movie{
	char MName[20];
	struct movieInfo obj1;
};
void main(){
	struct movie obj2;

	strcpy(obj2.MName,"kantara");
	strcpy(obj2.obj1.actor,"rishab");
	obj2.obj1.imdrate=200.22;

	printf("movie name:%s\n",obj2.MName);
	printf("actor name:%s\n",obj2.obj1.actor);
	printf("imdrate is:%f\n",obj2.obj1.imdrate);
}
