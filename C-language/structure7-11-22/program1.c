// passing struct object
#include<stdio.h>

struct demo{
	int x;
	float y;
};
void main(){
	struct demo obj={10,12.3f};

	int arr[]={10,20,30,40,50};
	printf("%p\n",obj);
	printf("%p\n",&obj);
	printf("%p\n",&obj.x);
	printf("%p\n",arr);
	printf("%p\n",&arr[0]);
}
