/*
 4. take no of rows from the user
a B c D
b C d E
c D e F
d E f G
 */


#include<stdio.h>
void main(){
	int rows;
	printf("enter the rows\n");
        scanf("%d",&rows);

	for(int i=1;i<=rows;i++){
		int ch1=96+i;
		int ch2=65+i;
		for(int j=1;j<=rows;j++){
			if(j%2==1){
				printf("%c\t",ch1);
				ch1=ch1+2;
			}else{
				printf("%c\t",ch2);
				ch2=ch2+2;
			}
		}
		printf("\n");
	}
}
