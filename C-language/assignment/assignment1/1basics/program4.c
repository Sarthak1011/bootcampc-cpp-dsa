// write a program to print even numbers 1-100


#include<stdio.h>
void main(){
	int num1,num2;
	printf("enter first and last numbers:\n");
	scanf("%d%d",&num1,&num2);
	printf("the even number betweem %d and %d is:\n",num1,num2);
	for(int i=num1;i<=num2;i++)
	{
		if(i%2==0){
	         	printf("%d ",i);
		}
	}
}

/*
output

enter first and last numbers:
1
100
the even number betweem 1 and 100 is:
2 4 6 8 10 12 14 16 18 20 22 24 26 28 30 32 34 36 38 40 42 44 46 48 50 52 54 56 58 60 62 64 66 68 70 72 74 76 78 80 82 84 86 88 90 92 94 96 98 100
 
 */ 
