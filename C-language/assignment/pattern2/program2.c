/*
 3 2 1 
 c b a
 3 2 1
 c b a
 */

#include<stdio.h>
void main(){
	int rows,cols;

	printf("enter the rows and colums\n");
	scanf("%d%d",&rows,&cols);

	for(int i=1;i<=rows;i++){
		int x=3;
		char ch='c';
		for(int j=1;j<=cols;j++){
			if(i%2==1){
				printf("%d\t",x);
				x--;
			}else{
				printf("%c\t",ch);
				ch--;
			}
		}
		printf("\n");
	}
}
