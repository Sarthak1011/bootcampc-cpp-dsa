/*
 Dailyflash 9

Write a realtime example of Doubly Linked list with all the 8 functions included in switch case 
Push the code and send a diagram to me
 */ 


#include<stdio.h>
#include<stdlib.h>
#include<string.h>

typedef struct songlist{
	struct songlist *prev;
	char sname[20];
	float min;
	struct songlist *next;
}sl;

sl *head=NULL;

sl *createnode(){
	sl *node=(sl *)malloc(sizeof(sl));
	getchar();
	node->prev=NULL;
	printf("enter the song name\n");
	fgets(node->sname,20,stdin);
	node->sname[strlen(node->sname)-1]='\0';
	printf("how many minutes\n");
	scanf("%f",&node->min);
	node->next=NULL;
	return node;
}

void addnode(){
	sl *node=createnode();

	if(head==NULL){
		head=node;
	}else{
		sl *temp=head;
		while(temp->next!=NULL){
			temp=temp->next;
		}
		temp->next=node;
		node->prev=temp;
	}
}
void addfirst(){
	sl *node=createnode();

	if(head==NULL){
		head=node;
	}else{
		node->next=head;
		head->prev=node;
		head=node;
	}
}
void addlast(){
	addnode();
}
int countnode(){
	int count=0;
	sl *temp=head;
	while(temp!=NULL){
		temp=temp->next;
		count++;
	}
	return count;
}

int addatpos(){
	int pos;
	printf("enter the position\n");
	scanf("%d",&pos);
	int count=countnode();

	if(pos<=0||pos>=count+2){
		printf("wrong position\n");
		return -1;
	}else{
		if(pos==1){
			addfirst();
		}else if(pos==count+1){
			addlast();
		}else{
			sl *temp=head;
			sl *node=createnode();
			while(pos-2){
				temp=temp->next;
				pos--;
			}
			node->next=temp->next;
			node->prev=temp;
			temp->next->prev=node;
			temp->next=node;
		}
	}
	return 0;
}
int printLL(){
	if(head==NULL){
		printf("linked list is empty\n");
		return -1;
	}else{
		sl *temp=head;
		while(temp->next!=NULL){
			printf("|%s->",temp->sname);
			printf("%f|->",temp->min);
			temp=temp->next;
		}
			printf("|%s->",temp->sname);
			printf("%f|\n",temp->min);
	}
	return 0;
}
int deletefirst(){
	if(head==NULL){
		printf("linked list is empty\n");
		return -1;
	}else{
		if(head->next==NULL){
			free(head);
			head=NULL;
		}else{
			head=head->next;
			free(head->prev);
			head->prev=NULL;
		}
	}
	return 0;
}
		

int deletelast(){
	if(head==NULL){
		printf("linked list is empty\n");
		return -1;
	}else{
		if(head->next==NULL){
			free(head);
			head=NULL;
		}else{
			sl *temp=head;
			while(temp->next->next!=NULL){
				temp=temp->next;
			}
			free(temp->next);
			temp->next=NULL;
		}
	}
	return 0;
}
int deleteatpos(){
	int pos;
	printf("enter the position\n");
	scanf("%d",&pos);

	int count =countnode();

	if(pos<=0||pos>count){
		printf("invalid position\n");
		return -1;
	}else{
		if(pos==1){
			deletefirst();
		}else if(pos==count){
			deletelast();
		}else{
			sl *temp=head;
			while(pos-2){
				temp=temp->next;
				pos--;
			}
			temp->next=temp->next->next;
			free(temp->next->prev);
			temp->next->prev=temp;
		}
			

		}
	return 0;
	
}
void main(){
	char choice;
	do {
		printf("1.addnode\n");
		printf("2.addfirst\n");
		printf("3.addlast\n");
		printf("4.addatpos\n");
		printf("5.printLL\n");
		
		printf("6.deletefirst\n");
		printf("7.deletelast\n");
		printf("8.deleteatpos\n");
		printf("enter your choice\n");
		int ch;
		scanf("%d",&ch);

		switch(ch){
			case 1:
				addnode();
				break;
			case 2:
				addfirst();
				break;
			case 3:
				addlast();
				break;
			case 4:
				addatpos();
				break;
			case 5:
				printLL();
				break;
			case 6:
				deletefirst();
				break;
			case 7:
				deletelast();
				break;
			case 8:
				deleteatpos();
				break;
			default :
			          printf("wrong choice\n");
		}


                      printf("do you want to continue\n");
		      getchar();
		      scanf("%c",&choice);
	}while(choice=='y'||choice=='Y');
}




			


