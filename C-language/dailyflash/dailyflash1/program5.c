/*
5.WAP takes a 2d array from the user and prints its addition of odd elements. 
 */

#include<stdio.h>

void main(){
	int rsize,csize;

	printf("enter the row and column size\n");

	scanf("%d%d",&rsize,&csize);

	int arr[rsize][csize];

	for(int i=0;i<rsize;i++){
		for(int j=0;j<csize;j++){
			scanf("%d",&arr[i][j]);
		}
	}
	
	int sum=0;
	for(int i=0;i<rsize;i++){
		for(int j=0;j<csize;j++){
			sum=sum+arr[i][j];
		}

	}
	printf("sum of array elements is %d\n",sum);
}
