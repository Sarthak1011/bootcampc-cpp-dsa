/*
 4.WAP to take a 3d array from the user and make an addition of the elements.
 */ 


#include<stdio.h>
void main(){
	int psize,rsize,csize;
	printf("enter the plane size,row size,col size\n ");
	scanf("%d%d%d",&psize,&rsize,&csize);

	int arr[psize][rsize][csize];

	printf("enter the array elements \n");
	for(int i=0;i<psize;i++){
		for(int j=0;j<rsize;j++){
			for(int k=0;k<csize;k++){
				scanf("%d",&arr[i][j][k]);
			}
		}
	}

	int sum=0;
	for(int i=0;i<psize;i++){
		for(int j=0;j<rsize;j++){
			for(int k=0;k<csize;k++){
				sum=sum+arr[i][j][k];	
			}
		}
	}
	printf("sum of array elements is %d\n",sum);
}
