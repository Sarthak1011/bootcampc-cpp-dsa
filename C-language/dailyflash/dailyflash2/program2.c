/*
 1. WAP to print different datatype of elements using Void pointer
int , char , float , double
 */ 
#include<stdio.h>
void main(){
	int a=10;
	char ch='A';
	float b=10.11f;
	double d=29.11;

	void *vptr[4]={&a,&ch,&b,&d};

	printf("%d\n",*(int *)vptr[0]);
	printf(" %c\n",*(char *)vptr[1]);
	printf("%f\n",*(float *)vptr[2]);
	printf("%lf\n",*(double *)vptr[3]);
}
