//
#include<stdio.h>
#include<stdlib.h>
#include<string.h>

struct country{
	char name[10];
	int scount;
	double income;
};

void main(){

	struct country *ptr1=(struct country *)malloc (sizeof(struct country));

	strcpy(ptr1->name,"INDIA");
	ptr1->scount=27;
	ptr1->income=123922.211;

	printf("country name:%s\n",ptr1->name);
	printf("state count :%d\n",ptr1->scount);
	printf("daily income:%f\n",ptr1->income);
}
